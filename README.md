this is the public version of the trackdescriptors base package.
For 3D support and annotation of context like blood vessel shape, diameter:
check out the trackdescriptors3D extension package, which also viszualizes using the RGL package.