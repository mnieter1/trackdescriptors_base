#' load an imaris csv position file
#'
#' attach the filenpath as column for reference
#'
#' @param path the path to the imaris export file
#'
#' @return the table imported into R with source column addition
#' @export
#'
#' @examples
#' path <- system.file("extdata", "some_experiments_cells_Position.csv", package = "trackdescriptors")
#' my_imaris_file_loaded_to_R <- load_an_imaris_position_file(path)
 load_an_imaris_position_file <- function(path){
  #
  current_table <- read.csv(file = path, header = TRUE, sep = ",", skip = 3, stringsAsFactors = FALSE)
  #
  current_table$path <- path

  # extract the filename and append as column
  # make path end the treatment
  all_path_is_chopped <- strsplit(x = current_table$path, split = "/")
  for(index_of_path in 1:length(current_table$path)){
    # just the last to stand can pass
    if(index_of_path == 1){
      #start collection
      collection <- all_path_is_chopped[[index_of_path]][length(all_path_is_chopped[[index_of_path]])]
    }else{
      collection <- c(collection, all_path_is_chopped[[index_of_path]][length(all_path_is_chopped[[index_of_path]])])
    }

  }
  # make file name treatment
  current_table$filename <- collection



   # return the table
   return(current_table)
 }
