
#' translate all track start positions to origin
#'
#' @param coordinates_list the coordinates as 2D- or 3D-coordinate lists
#' @param track_id_list identifiers for separating the coordinates into tracks
#'
#' @return a coordinate list with all tracks with their start centered at origin
#' @export
#'
#' @examples
#' # enhancer
#' plot_all_tracks_transposed_w_start_to_origin(
#'  coordinates_list = testcase_tracklist_w_clusters_j_Wnt5a[ , c("x.position", "y.position")],
#'  track_id_list = testcase_tracklist_w_clusters_j_Wnt5a[ , "trackid"],
#'  main_title = "Wnt5a - enhancer")
#'  # DMSO control
#'  plot_all_tracks_transposed_w_start_to_origin(
#'  coordinates_list = testcase_tracklist_w_clusters_j_DMSO[ , c("x.position", "y.position")],
#'  track_id_list = testcase_tracklist_w_clusters_j_DMSO[ , "trackid"],
#'  main_title = "DMSO - control")
#'  # inhibitor Box5
#'  plot_all_tracks_transposed_w_start_to_origin(
#'  coordinates_list = testcase_tracklist_w_clusters_j_Box5[ , c("x.position", "y.position")],
#'  track_id_list = testcase_tracklist_w_clusters_j_Box5[ , "trackid"],
#'  main_title = "Box5 - inhibitor")
#'  #'  # inhibitor Wnt-C59
#'  plot_all_tracks_transposed_w_start_to_origin(
#'  coordinates_list = testcase_tracklist_w_clusters_j_WntC59[ , c("x.position", "y.position")],
#'  track_id_list = testcase_tracklist_w_clusters_j_WntC59[ , "trackid"],
#'  main_title = "Wnt-C59 - inhibitor")


translate_all_track_start_positions_to_origin <- function (coordinates_list, track_id_list) {

  # get the trackID list

  track_list <- unique(track_id_list)

  # get the number of tracks
  nr_of_tracks <- length(track_list)

  # collect x and y translated
  x_collector <- NA
  y_collector <- NA


  for(i in 1:nr_of_tracks){

    current_coordinate_index_list <- which(track_id_list %in% track_list[i])


    # extract starting point
    start_x = coordinates_list[current_coordinate_index_list[1], 1]
    start_y = coordinates_list[current_coordinate_index_list[1], 2]

    current_x_translated <- coordinates_list[current_coordinate_index_list, 1] - start_x
    current_y_translated <- coordinates_list[current_coordinate_index_list, 2] - start_y

    # if first
    if(i == 1){
      x_collector <- current_x_translated
      y_collector <- current_y_translated

    }  else {
      x_collector <- c(x_collector, current_x_translated)
      y_collector <- c(y_collector, current_y_translated)

    }



  }

  # put the coord together
  answer <- cbind(x_collector, y_collector)
  colnames(answer) <- colnames(coordinates_list)

  return(answer)
}
