#' plot the clustering results of a set of
#' specified descriptor vectors
#'
#' @aliases hcluster
#' @param description_vector_per_trackid a matrix containing vectors containing
#'   the descriptions of a track columnwise
#' @param number_of_clusters defines number of different clusters
#'
#' @return a plot with a colored dendrogram to show specified number of clusters
#' @export
#' @importFrom magrittr %>%
#' @importFrom dendextend set
#'
#' @examples
#' plot_cluster_colored_tree(
#'    description_vector_per_trackid = test_descriptor_with100divisions,
#'    number_of_clusters = 9)
plot_cluster_colored_tree <- function (
  description_vector_per_trackid,
  number_of_clusters){


  # clustering start ------------------------------------------

  #hclust of tracks
  # prepare hierarchical cluster
  hc = hclust(dist(t(as.matrix(description_vector_per_trackid)),method="manhattan"), method = "ward.D") #  "ward.D", "ward.D2", "single", "complete", "average" (= UPGMA),
  # "mcquitty" (= WPGMA), "median" (= WPGMC) or "centroid" (= UPGMC).


  index_list_for_ordered_depiction_as_with_dendrogram <- hc$order
  label_list_for_ordered_depiction_as_with_dendrogram <- hc$labels


  hcd <- as.dendrogram(hc, hang = -1, labels = FALSE)


  # extract the groups like in the dendrogram left is first, horzontal it's the lowerest
  groups <- dendextend::cutree(hcd, k=number_of_clusters,
                               order_clusters_as_data = FALSE) # cut tree into number_of_clusters <-  clusters

  #define color blind palette (http://www.cookbook-r.com/Graphs/Colors_(ggplot2)/)
  # The palette with black:
  #cbb_palette <- c("#000000", "#E69F00", "#56B4E9", "#009E73", "#F0E442", "#0072B2", "#D55E00", "#CC79A7")
  # The palette with grey:
  #cbb_palette <- c("#999999", "#E69F00", "#56B4E9", "#009E73", "#F0E442", "#0072B2", "#D55E00", "#CC79A7")
  # colorfull color blind palette (http://dr-k-lo.blogspot.de/2013/07/a-color-blind-friendly-palette-for-r.html)
  cbb_palette <- c("#009E73", "#e79f00", "#9ad0f3", "#0072B2", "#D55E00",
                   "#CC79A7", "#F0E442", "#000000")

  # To use for fills, add
  # scale_fill_manual(values=cbPalette)

  # To use for line and point colors, add
  #scale_colour_manual(values=cbPalette)



  hcd %>% set("labels_cex", 2) %>%
    set("labels_col", value = c(2:(number_of_clusters+1)), k=number_of_clusters) %>%
    #set("branches_k_col", value = c(2:(number_of_clusters+1)), k=number_of_clusters) %>%
    set("branches_k_col", value = cbb_palette, k=number_of_clusters) %>%

    # set("branches_lty", 3) %>% # dashed line

    set("branches_lwd", 4) %>%
    set("labels", NA) %>%

    plot(main = paste(number_of_clusters, " main classes"), horiz=T)


}
