#' per track start end distance
#'
#' @param coordinates_list the coordinates as 2D- or 3D-coordinate lists
#' @param track_id_list identifiers for separating the coordinates into tracks
#'
#' @return a coordinate list with distance vectors,
#'        one per track speifying displacement
#' @export
#'
#' @examples
#' testcase_displacements <- displacement_vector_per_track(
#'  coordinates_list = testcase_tracklist[ , c("x.position", "y.position")],
#'  track_id_list = testcase_tracklist[ , "trackid"])



displacement_vector_per_track <- function (coordinates_list, track_id_list) {

  # get the trackID list
  track_list <- unique(track_id_list)

  # get the number of tracks
  nr_of_tracks <- length(track_list)


  # collect x and y translated
  x_collector <- NA
  y_collector <- NA


  for(i in 1:nr_of_tracks){

    current_coordinate_index_list <- which(track_id_list %in% track_list[i])


    # extract starting point
    start_x <- coordinates_list[current_coordinate_index_list[1], 1]
    start_y <- coordinates_list[current_coordinate_index_list[1], 2]
    # extracting end points
    end_x <- coordinates_list[current_coordinate_index_list[length(current_coordinate_index_list)], 1]
    end_y <- coordinates_list[current_coordinate_index_list[length(current_coordinate_index_list)], 2]
    # calculating displacement
    current_x_displacement <-  end_x - start_x
    current_y_displacement <- end_y - start_y

    # if first
    if(i == 1){
      x_collector <- current_x_displacement
      y_collector <- current_y_displacement

    }  else {
      x_collector <- c(x_collector, current_x_displacement)
      y_collector <- c(y_collector, current_y_displacement)

    }


  }
    # put the coordinates vectors together
    answer <- data.frame(track_list, x_collector, y_collector)
    colnames(answer) <- c("trackid", colnames(coordinates_list))
    rownames(answer) <- unique(track_list)

    return(answer)

}
