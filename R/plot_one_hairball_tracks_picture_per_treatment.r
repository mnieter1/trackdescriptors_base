#' plot one hairball picture per condition
#'
#' @param coordinates_list identifiers for separating the coordinates into tracks
#' @param track_id_list identifiers for separating the coordinates into tracks annotating the \code{coordinates_list}
#' @param conditions_list a list with the differents conditions annotating the \code{coordinates_list}
#' @param outreach the max distance defining the outreach of the plot from the origin
#'
#' @return as many plots as there are different conditions found in the \code{conditions_list}
#' @export
#'
#' @examples
#' # plot as many pictures as there are treatments
#' plot_one_hairball_tracks_picture_per_treatment(
#'    coordinates_list = testcase_tracklist[ , c("x.position", "y.position")],
#'    track_id_list = testcase_tracklist[ , "trackid"],
#'    conditions_list = testcase_tracklist[ , "treatment"],
#'    outreach = 300)
#'
#'
#' # example with just the DMSO subset creates single plot
#' plot_one_hairball_tracks_picture_per_treatment(
#'      coordinates_list = DMSO_control_tracks[ , c("x.position", "y.position")],
#'      track_id_list = DMSO_control_tracks[ , "trackid"],
#'      conditions_list = DMSO_control_tracks[ , "treatment"],
#'      outreach = 300)

plot_one_hairball_tracks_picture_per_treatment <- function (coordinates_list, track_id_list, conditions_list, outreach = 300) {


  # for each condition make a single plot
  conditions_are <- unique(conditions_list)
  nr_of_conditions <- length(conditions_are)

  for(condition_index in 1:nr_of_conditions) {
    # make subset
    pointer_list_to_condition <- which(as.character(conditions_list) %in% as.character(conditions_are[condition_index]))

    # make the plot
    plot_all_tracks_transposed_w_start_to_origin(
      coordinates_list = coordinates_list[pointer_list_to_condition, ],
      track_id_list = track_id_list[pointer_list_to_condition],
      main_title = conditions_are[condition_index],
      outreach = outreach)

  }


}
