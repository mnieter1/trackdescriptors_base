#' relative angle to direction class
#'
#' @param query_tracks_with_relative_angle a track table containing relative angle degrees between vectors (ref and track)
#' @param name_of_angle_column the name of the column with the angles
#'
#' @return the table with direction class encoded as -1 against; 0 perpendicular; 1 with the flow
#' @export
#'
#' @examples
relative_angle_to_direction_class <- function(query_tracks_with_relative_angle, name_of_angle_column){


  #take angle column
  for (coord_index in 1:dim(query_tracks_with_relative_angle)[1]) {

    current_class_label <- NA

    #if < 45° its along so return 1
    if(query_tracks_with_relative_angle[coord_index, name_of_angle_column] < 45){
      current_class_label <- 1
    }else if(query_tracks_with_relative_angle[coord_index, name_of_angle_column] >= 45
             & query_tracks_with_relative_angle[coord_index, name_of_angle_column] <= 135){
      #if >= 45° AND <= 135° its perpendicular so return 0
      current_class_label <- 0
    }else if(query_tracks_with_relative_angle[coord_index, name_of_angle_column] > 135){
      #if > 135° its oposite so return -1
      current_class_label <- -1
    }

    # collect the results
    if(coord_index == 1){
      collector <- current_class_label
    }else{
      collector <- c(collector, current_class_label)
    }

  }
    # add the info to the query table
    query_tracks_with_relative_angle$relative_angle_class <- collector

  return (query_tracks_with_relative_angle)
}
