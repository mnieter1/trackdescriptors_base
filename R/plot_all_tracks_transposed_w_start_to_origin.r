
#' plot all tracks origin translated start positions
#'
#' @param coordinates_list the coordinates as 2D- or 3D-coordinate lists
#' @param track_id_list identifiers for separating the coordinates into tracks
#' @param outreach the max distance defining the outreach of the plot from the origin
#' @param main_title the main title of the plot (e.g. mentioning treatment)
#'
#' @return a plot with the tracks all with their start centered at origin
#' @export
#'
#' @examples
#' # enhancer
#' plot_all_tracks_transposed_w_start_to_origin(
#'  coordinates_list = testcase_tracklist_w_clusters_j_Wnt5a[ ,
#'    c("x.position", "y.position")],
#'  track_id_list = testcase_tracklist_w_clusters_j_Wnt5a[ , "trackid"],
#'  main_title = "Wnt5a - enhancer")
#'  # DMSO control
#'  plot_all_tracks_transposed_w_start_to_origin(
#'  coordinates_list = testcase_tracklist_w_clusters_j_DMSO[ ,
#'    c("x.position", "y.position")],
#'  track_id_list = testcase_tracklist_w_clusters_j_DMSO[ , "trackid"],
#'  main_title = "DMSO - control")
#'  # inhibitor Box5
#'  plot_all_tracks_transposed_w_start_to_origin(
#'  coordinates_list = testcase_tracklist_w_clusters_j_Box5[ ,
#'    c("x.position", "y.position")],
#'  track_id_list = testcase_tracklist_w_clusters_j_Box5[ , "trackid"],
#'  main_title = "Box5 - inhibitor")
#'  #'  # inhibitor Wnt-C59
#'  plot_all_tracks_transposed_w_start_to_origin(
#'  coordinates_list = testcase_tracklist_w_clusters_j_WntC59[ ,
#'    c("x.position", "y.position")],
#'  track_id_list = testcase_tracklist_w_clusters_j_WntC59[ , "trackid"],
#'  main_title = "Wnt-C59 - inhibitor")


plot_all_tracks_transposed_w_start_to_origin <- function (coordinates_list, track_id_list, main_title = "", outreach = 300) {

  # get the trackID list

  track_list <- unique(track_id_list)

  # get the number of tracks
  nr_of_tracks <- length(track_list)

  # reach
  reach <- outreach
  # plot empty plot
  plot(1, type="n", axes=T, xlab="", ylab="", xlim = c(-reach, reach), ylim = c(-reach, reach), main = main_title)

  #plot all points

  #plot(coordinates_list, col = "gray", pch = 19, main = condition_name, ylim = plotting_range, xlim = plotting_range)

  for(i in 1:nr_of_tracks){

    current_coordinate_index_list <- which(track_id_list %in% track_list[i])



        #lines(knime.in$"x-Koordinate"[current_coordinate_index_list],knime.in$"y-Koordinate"[current_coordinate_index_list])

    #print(paste(color_palette_to_use[as.numeric(as.character(cluster_ids[current_coordinate_index_list[1]]))], cluster_ids[current_coordinate_index_list[1]]))

    # extract starting point
    start_x = coordinates_list[current_coordinate_index_list[1], 1]
    start_y = coordinates_list[current_coordinate_index_list[1], 2]

    lines(x = coordinates_list[current_coordinate_index_list, 1] - start_x ,
          y = coordinates_list[current_coordinate_index_list, 2] - start_y,
          col = adjustcolor("black", 0.3),
          lwd = 3)

  }
  # add ablines
  abline(v=0,col="red",lty=3, lwd = 5)
  abline(h=0,col="red",lty=3, lwd = 5)

  mtext(paste("n tracks =", nr_of_tracks, sep=""), side = 3, line = 0, adj=1)
}
