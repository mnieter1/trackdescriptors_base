
#' Distribution Membership As Simple Horizontal Bar Plot with Added Labels
#'
#' \code{show_cluster_distribution_per_condition} returns a bar plot for the
#' depiction of cluster member distributions per condition
#'
#' @param matrix_with_the_counts_or_percentages a table containing the cluster
#'   distribution per condition; oriented columnwise. Like the output of
#'   \code{\link{member_counts_per_cluster}}
#'
#' @return a bar plot with one sub barplot/fingerprint per condition with the
#'   colors used also in the clustering
#'   \code{\link{plot_cluster_colored_tree_and_representative_tracks_and_fingerprints}}
#'
#'
#' @export
#'
#' @examples
#' # calling the cluster distribution function on the reference track data input
#' test_output_for_6_clusters <- member_counts_per_cluster(
#'                  track_ids_w_treatment_class = unique(
#'                  testcase_tracklist[ , c("treatment", "trackid")]),
#'                  number_of_clusters = 6, descriptors = test_descriptor_with100divisions)
#' # calling the barplot with the matrix
#' show_cluster_distribution_per_condition(test_output_for_6_clusters)
show_cluster_distribution_per_condition <- function (matrix_with_the_counts_or_percentages) {

  nr_of_colors_to_use = dim(matrix_with_the_counts_or_percentages)[1]

  # order descending cluster id / which is the id in rownames
  matrix_to_plot <- matrix_with_the_counts_or_percentages[ nr_of_colors_to_use:1, ]

  cbbPalette <- c("#009E73", "#e79f00", "#9ad0f3", "#0072B2", "#D55E00",
                "#CC79A7", "#F0E442", "#000000")


  streched_cbb_palette <- array(cbbPalette, dim=c(nr_of_colors_to_use, 1))

  # invert because clusters come from bottom in turned dendrogram
  color_palette_to_use = streched_cbb_palette[nr_of_colors_to_use:1]

  barplot(as.matrix(matrix_to_plot), main="distribution of class membership regarding conditions", horiz=FALSE, beside=TRUE, col=color_palette_to_use)

}
