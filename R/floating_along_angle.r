#' Determine angle of direction and speed overlay
#'
#' determines the overlay of the query vectors,
#' indicating relative parallel or antiparellel movement by using scalar product summary of concordance
#' and scaling the relative movemement depending on the flow reference displacement in each dimension,
#'  thus focusing on dimensions where there is actually a flow, downscaling side drift dimensions
#'
#' @param track_x_y coordinates x y and optional z, or even more
#' @param flow_x_y coordinates as above
#'
#' @return for each dimension the overlay determined by division of components
#' @export
#'
#' @examples
#' # none yet
floating_along_angle <-
  function(track_x_y, flow_x_y) {
    # retrieve angle between two vector
    angle_inbetween_directions <- rad2deg(atan2(y = as.numeric(track_x_y[2]), x = as.numeric(track_x_y[1]))
            - atan2(y = as.numeric(flow_x_y[2]), x = as.numeric(flow_x_y[1])))
    # fix bigger than 180
    if(angle_inbetween_directions > 180){
      angle_inbetween_directions <- angle_inbetween_directions -360
    }else if(angle_inbetween_directions < -180){
      angle_inbetween_directions <- angle_inbetween_directions +360
    }
    angle_inbetween_directions <- abs(angle_inbetween_directions)
    # make the scalar by dividing the track vector component by the flow component
    #relative_percentage_floating <- track_x_y / flow_x_y
    # scale the relative percentage accoring to flow vector direction length in the x y and z
    #total_length_flow <- sum(flow_x_y)
    #relative_percentage_floating_scaled_to_importance <- relative_percentage_floating * (flow_x_y/ total_length_flow)
    # fix the ones with NA results
    #relative_percentage_floating_scaled_to_importance[is.na(relative_percentage_floating_scaled_to_importance)] <- 0

    return(angle_inbetween_directions)
  }
