#' transform track coordinate_list to correlation descriptors
#'
#' @param coordinate_list 2D- or 3D-coordinate lists
#' @param track_ids identifyers for separating the coordinate_list into tracks
#' @param nr_of_divisions how many bins/divisions should be used in the descriptor
#' @param distance_measure either "euclidean" or "manhattan" used in the distance function
#'
#' @return a matrix with the descriptors and track identifyers
#' @export
#'
#' @examples
#' # define nr of bins
#' nr_of_divisions <- 100
#' # define a coordinate table
#' coordinate_list <- testcase_tracklist[ , c("x.position", "y.position")]
#' # define the trackID list to separate different tracks in the coordinate field
#' track_ids <- testcase_tracklist[ , "trackid"]
#' # call the autocorrelation vector maker
#' test_descriptor_with100divisions <- autocorrelation_vector_per_trackid(
#'                                    coordinate_list,
#'                                    track_ids,
#'                                    nr_of_divisions,
#'                                    distance_measure = "manhattan")
autocorrelation_vector_per_trackid <- function (coordinate_list, track_ids, nr_of_divisions, distance_measure = "manhattan") {
  #distance_measure = "euclidean" or "manhattan"

  # get the number of dimensions aka number of coordinate columns x y z ...
  nr_of_dimensions = dim(coordinate_list)[2]

  # to get the list of all tracks to loop
  unique_tracks <- unique(track_ids)


  # get max of values
  max_value <- -Inf
  #print(max_value)

  # get min of values
  min_value <- Inf
  #print(min_value)



  for (track_index in 1:length(unique_tracks)) {

    # make sure there are at least 2 points otherwise skip for evaluation
    if(length(coordinate_list[which(track_ids == unique_tracks[track_index]), 1]) != 1){
      # subtable with just one track
      single_track <-
        coordinate_list[which(track_ids == unique_tracks[track_index]),]

      single_track_distance_matrix <-
        as.matrix(dist(single_track, method = distance_measure, diag = TRUE))

      # get max of values
      if (max_value < max(single_track_distance_matrix))
        max_value <- max(single_track_distance_matrix)

      # get min of values
      if (min_value > min(single_track_distance_matrix))
        min_value <- min(single_track_distance_matrix)

    }

  }

  for (track_index in 1:length(unique_tracks)) {

    # answer_vector with length nr_of_divisions, initialized to 0
    answer_vector <- rep(0, nr_of_divisions)

    # make sure there are at least 2 points otherwise skip for evaluation
    if (length(coordinate_list[which(track_ids == unique_tracks[track_index]), 1]) != 1) {
      # subtable with just one track
      single_track <-
        coordinate_list[which(track_ids == unique_tracks[track_index]),]

      single_track_distance_matrix <-
        as.matrix(dist(single_track, method = distance_measure, diag = TRUE))



      for (i in 1:length(single_track_distance_matrix[,1])) {
        for (j in 1:length(single_track_distance_matrix[1,])) {
          #where to count up
          bin_index <-
            bin_drop_position(single_track_distance_matrix[j,i], min_value, max_value, nr_of_divisions)
          # increase by one
          answer_vector[bin_index] <- answer_vector[bin_index] + 1;

        }
      }
    }
    #collect the descriptor vector
    if (track_index == 1) {
      collector_of_vectors <- as.matrix(answer_vector)
    }else{
      collector_of_vectors <-
        cbind(as.matrix(collector_of_vectors), as.matrix(answer_vector))
    }


  }

  colnames(collector_of_vectors) <- unique_tracks
  return (collector_of_vectors)
}
