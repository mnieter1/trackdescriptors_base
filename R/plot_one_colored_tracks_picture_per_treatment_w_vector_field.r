#' plot one colored tracks picture per condition with vector field
#'
#' @param coordinates_list identifiers for separating the coordinates into tracks
#' @param track_id_list identifiers for separating the coordinates into tracks
#'  annotating the \code{coordinates_list}
#' @param cluster_ids a list with the cluster_id for each track position
#'  annotating the \code{coordinates_list}
#' @param conditions_list a list with the differents conditions
#'  annotating the \code{coordinates_list}
#' @param mark_start_end a boolean stating if start and endpoints should be marked
#' @param nr_of_x_zones how many zones along x axis
#' @param nr_of_y_zones how many zones along y axis
#' @param lims_to_use the limits for zoning e.g. c(xlower, xupper, ylower, yupper)
#' @param arrow_color the color to use; default "black"
#' @param arrow_lwd the line width of the arrow; default is 4
#'
#' @return as many plots as there are different conditions
#'  found in the \code{conditions_list}
#' @export
#'
#' @examples
#' # define parameters
#' # nr of zones in x
#' nr_of_x_zones <- 6
#' # nr of zones in y
#' nr_of_y_zones <- 6
#' # the limits of the zone window
#' lims_to_use <- c(0, 1300, 0, 1100)
#' # first get the track cluster annotation
#' # annotate the longer coordinate list by annotating the track id list
#' # from the track annotation with the cluster ids from the hclust
#' testcase_trackids_with_clusterids_for_6clusters <-
#'    track_ids_with_cluster_ids_attached <- attach_cluster_ids_to_track_ids(
#'        track_ids = testcase_tracklist[ , "trackid"],
#'        hclust_result = example_hclust_result_for_autocorrelation_descriptor,
#'        number_of_clusters = 6)
#' # then depict the clusters as color info
#' plot_one_colored_tracks_picture_per_treatment_w_vector_field(
#'    coordinates_list = testcase_tracklist[ , c("x.position", "y.position")],
#'    track_id_list = testcase_tracklist[ , "trackid"],
#'    conditions_list = testcase_tracklist[ , "treatment"],
#'    cluster_ids = as.character(
#'    testcase_trackids_with_clusterids_for_6clusters[ , "clusterid"]),
#'    nr_of_x_zones = nr_of_x_zones,
#'    nr_of_y_zones = nr_of_y_zones,
#'    lims_to_use = lims_to_use)
#'
#'
#' # example with just the DMSO subset creates single plot
#' plot_one_colored_tracks_picture_per_treatment_w_vector_field(
#'      coordinates_list = DMSO_control_tracks[ , c("x.position", "y.position")],
#'      track_id_list = DMSO_control_tracks[ , "trackid"],
#'      conditions_list = DMSO_control_tracks[ , "treatment"],
#'      cluster_ids = DMSO_track_ids_with_cluster_ids_attached[ , "clusterid"],
#'      mark_start_end = TRUE,
#'    nr_of_x_zones = nr_of_x_zones,
#'    nr_of_y_zones = nr_of_y_zones,
#'    lims_to_use = lims_to_use)

plot_one_colored_tracks_picture_per_treatment_w_vector_field <-
  function (coordinates_list,
            track_id_list,
            cluster_ids,
            conditions_list,
            mark_start_end = TRUE,
            nr_of_x_zones = 15,
            nr_of_y_zones = 15,
            lims_to_use = c(0, 400, 0, 400),
            arrow_color = "black",
            arrow_lwd = 4) {

  # figure out how many clusters there where
  nr_of_colors_to_use = length(unique(cluster_ids))
  # pass the above in the plot functions below to get correct color
  # representation even if some clusters are not populated

  # for each condition make a single plot
  conditions_are <- unique(conditions_list)
  nr_of_conditions <- length(conditions_are)

  for(condition_index in 1:nr_of_conditions) {
    # make subset
    pointer_list_to_condition <- which(as.character(conditions_list) %in% as.character(conditions_are[condition_index]))

    # make the plot
    plot_tracks_with_cluster_color_from_hclust(
      coordinates_list = coordinates_list[pointer_list_to_condition, ],
      track_id_list = track_id_list[pointer_list_to_condition],
      cluster_ids = cluster_ids[pointer_list_to_condition],
      condition_name = conditions_are[condition_index],
      nr_of_clusters = nr_of_colors_to_use,
      mark_start_end = mark_start_end)
    # make the trajectory table for arrow function
    current_track_table <- data.frame(
      coordinates_list[pointer_list_to_condition, ],
      track_id_list[pointer_list_to_condition])
    colnames(current_track_table) <- c("x.position", "y.position", "trackid")
    # add the vector field depictions
    add_arrows_with_mean_start_end_displacement_per_zone(
      trajectory_table = current_track_table,
      nr_of_x_zones = nr_of_x_zones,
      nr_of_y_zones = nr_of_y_zones,
      lims_to_use = lims_to_use,
      arrow_color = arrow_color,
      arrow_lwd = arrow_lwd)
  }


}
