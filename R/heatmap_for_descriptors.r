
#' plot heatmap of unsorted descriptors and tracks / samples
#'
#' @param descriptor_table a table with descriptor vectors per row
#' @param treatment_ids the treatment ids for the tracks / samples
#'  to tag the sidebar accordingly
#'
#' @return a heatmap.2 plot with the descriptors rowwise
#' and tracks kept in the order specified in the matrix
#' @export
#'
#' @examples
#' tracklist_from_descriptor <- colnames(test_descriptor_with100divisions)
#' heatmap_descriptors_and_tracks_unsorted(descriptor_table =
#'    t(test_descriptor_with100divisions),
#'    treatment_ids =
#'    mapping_list_tracks_treatments[tracklist_from_descriptor , "treatment"])
heatmap_descriptors_and_tracks_unsorted <- function(descriptor_table, treatment_ids){

  par(mar=c(5, 5, 5, 5))

#########################################################
### B) Reading in data and transform it into matrix format
#########################################################

#data <- read.csv("../datasets/heatmaps_in_r.csv", comment.char="#")
data <- descriptor_table
#rnames <- data[ , 1]                            # assign labels in column 1 to "rnames"
mat_data <- data.matrix(data)  # transform column 2-5 into a matrix
#rownames(mat_data) <- rnames                  # assign row names

row_distance = dist(mat_data, method = "manhattan")
row_cluster = hclust(row_distance, method = "complete")
col_distance = dist(t(mat_data), method = "manhattan")
col_cluster = hclust(col_distance, method = "complete")


#scale if wanted to sqrt space for coloring
mat_data <- sqrt(mat_data)

#########################################################
### C) Customizing and plotting the heat map
#########################################################

# creates a own color palette from red to green
#my_palette <- colorRampPalette(c("red", "yellow", "green"))(n = 299)
my_palette <- colorRamps::matlab.like(500)

cbbPalette <- c("#000000", "#009E73", "#e79f00", "#9ad0f3", "#0072B2", "#D55E00",
                "#CC79A7", "#F0E442")

# check which experiments are in the list
the_list_with_treatments <- unique(treatment_ids)
nr_of_treatments = length(unique(treatment_ids))

# build a vector with the colors corresponding to the experiments
color_per_experiments <- rep("#000000", length(treatment_ids))
for (current_treatment in 1:nr_of_treatments) {
  # override the default by the matching color
  color_per_experiments[which(treatment_ids == the_list_with_treatments[current_treatment])] <- cbbPalette[current_treatment]
}



experimentsAsColors = cbind(cbbPalette[levels(treatment_ids)])




# (optional) defines the color breaks manually for a "skewed" color transition
col_breaks = c(seq(-1,0,length=100),  # for red
               seq(0,0.8,length=100),              # for yellow
               seq(0.8,1,length=100))              # for green

gplots::heatmap.2(mat_data,
          #  cellnote = mat_data,  # same data set for cell labels
          #  main = "Correlation", # heat map title
          notecol = "black",      # change font color of cell labels to black
          density.info = "none",  # turns off density plot inside color legend
          trace = "none",         # turns off trace lines inside the heat map
          #margins = c(3,3),     # widens margins around plot
          col = my_palette,       # use on color palette defined earlier
          #  breaks=col_breaks,    # enable color transition at specified limits
          #Rowv = as.dendrogram(row_cluster), # apply default clustering method
          #Colv = as.dendrogram(col_cluster). # apply default clustering method
          dendrogram ="none",     # only draw a row dendrogram
          Colv ="NA",# turn off column clustering
          Rowv = "NA",
          scale="none",
          #  RowSideColors=as.character(knime.in$"experimentTypeColor")
          RowSideColors = color_per_experiments,
          labRow = NA,
          key = T,
          keysize = 1
)



par(lend = 1)           # square line ends for the color legend
legend("topright",      # location of the legend on the heatmap plot
       legend = unique(treatment_ids), # category labels
       col = cbbPalette[1:nr_of_treatments],  # color key
       lty= 1,             # line style
       lwd = 10            # line width
)
}



#' plot heatmap of unsorted descriptors and sort the tracks / samples
#'
#' @param descriptor_table a table with descriptor vectors per row
#' @param treatment_ids the treatment ids for the tracks / samples
#'  to tag the sidebar accordingly
#'
#' @return a heatmap.2 plot with the descriptors rowwise and tracks sorted
#' @export
#'
#' @examples
#'
#' tracklist_from_descriptor <- colnames(test_descriptor_with100divisions)
#' heatmap_descriptors_unsorted_and_tracks_sorted(descriptor_table =
#'    t(test_descriptor_with100divisions),
#'    treatment_ids =
#'    mapping_list_tracks_treatments[tracklist_from_descriptor , "treatment"])
heatmap_descriptors_unsorted_and_tracks_sorted <- function(descriptor_table, treatment_ids){

  #par(mar = c(5, 5, 5, 5), oma = c(0, 0, 10, 5))

  #########################################################
  ### B) Reading in data and transform it into matrix format
  #########################################################

  #data <- read.csv("../datasets/heatmaps_in_r.csv", comment.char="#")
  data <- descriptor_table
  #rnames <- data[ , 1]                            # assign labels in column 1 to "rnames"
  mat_data <- data.matrix(data)  # transform column 2-5 into a matrix
  #rownames(mat_data) <- rnames                  # assign row names

  row_distance = dist(mat_data, method = "manhattan")
  row_cluster = hclust(row_distance, method = "ward.D")
  col_distance = dist(t(mat_data), method = "manhattan")
  col_cluster = hclust(col_distance, method = "ward.D")


  #scale if wanted to sqrt space for coloring
  mat_data <- sqrt(mat_data)

  #########################################################
  ### C) Customizing and plotting the heat map
  #########################################################

  # creates a own color palette from red to green
  #my_palette <- colorRampPalette(c("red", "yellow", "green"))(n = 299)
  my_palette <- colorRamps::matlab.like(500)

  cbbPalette <- c("#000000", "#009E73", "#e79f00", "#9ad0f3", "#0072B2", "#D55E00",
                  "#CC79A7", "#F0E442")

  # check which experiments are in the list
  the_list_with_treatments <- unique(treatment_ids)
  nr_of_treatments = length(unique(treatment_ids))

  # build a vector with the colors corresponding to the experiments
  color_per_experiments <- rep("#000000", length(treatment_ids))
  for (current_treatment in 1:nr_of_treatments) {
    # override the default by the matching color
    color_per_experiments[which(treatment_ids == the_list_with_treatments[current_treatment])] <- cbbPalette[current_treatment]
  }



  experimentsAsColors = cbind(cbbPalette[levels(treatment_ids)])

  # use the clustering also used with the tree
  #distance = dist(mat_data, method = "manhattan")
  #cluster = hclust(distance, method = "ward.D")


  # (optional) defines the color breaks manually for a "skewed" color transition
  col_breaks = c(seq(-1,0,length=100),  # for red
                 seq(0,0.8,length=100),              # for yellow
                 seq(0.8,1,length=100))              # for green

  gplots::heatmap.2(mat_data,
                    #  cellnote = mat_data,  # same data set for cell labels
                    #  main = "Correlation", # heat map title
                    notecol = "black",      # change font color of cell labels to black
                    density.info = "none",  # turns off density plot inside color legend
                    trace = "none",         # turns off trace lines inside the heat map
                    margins=c(3, 2),     # widens margins around plot
                    col = my_palette,       # use on color palette defined earlier
                    #  breaks=col_breaks,    # enable color transition at specified limits
                    Rowv = as.dendrogram(row_cluster), # apply default clustering method
                    #Colv = as.dendrogram(col_cluster). # apply default clustering method
                    dendrogram = "row",  #"none",     # only draw a row dendrogram
                    Colv ="NA",# turn off column clustering
                    #Rowv = as.dendrogram(cluster), # apply default clustering method
                    #Colv = as.dendrogram(cluster)), # apply default clustering method
                    #Rowv = "NA",
                    scale="none",
                    #  RowSideColors=as.character(knime.in$"experimentTypeColor")
                    RowSideColors = color_per_experiments,
                    labRow = NA,
                    key = T,
                    keysize = 1
  )



  par(lend = 1)           # square line ends for the color legend
  legend("topright",      # location of the legend on the heatmap plot
         legend = unique(treatment_ids), # category labels
         col = cbbPalette[1:nr_of_treatments],  # color key
         lty= 1,             # line style
         lwd = 10            # line width
  )
}

