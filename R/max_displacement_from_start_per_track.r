#' per track max displacement from start position
#'
#' @param coordinates_list the coordinates as 2D- or 3D-coordinate lists
#' @param track_id_list identifiers for separating the coordinates into tracks
#'
#' @return a max displacement list
#'        with the maximal distance a particle moved from startposition of trajectory,
#'        one per track
#' @export
#'
#' @examples
#' max_displacement_for_testcase <- max_displacement_from_start_per_track(
#'  coordinates_list = testcase_tracklist[ , c("x.position", "y.position")],
#'  track_id_list = testcase_tracklist[ , "trackid"])
max_displacement_from_start_per_track <- function (coordinates_list, track_id_list) {

  # get the trackID list
  track_list <- unique(track_id_list)

  # get the number of tracks
  nr_of_tracks <- length(track_list)


  # collect path lenth per track
  max_displacement_per_track_collector <- NA


  for(i in 1:nr_of_tracks){

    current_coordinate_index_list <- which(track_id_list %in% track_list[i])

    # get the distance matrix
    current_distmatrix <- as.matrix(dist(coordinates_list[current_coordinate_index_list, ],
                                         method = "euclidian"))

    current_max_displacement_per_track <- max(as.numeric(current_distmatrix[ , 1]))


    # if first
    if(i == 1){
      max_displacement_per_track_collector <- as.numeric(current_max_displacement_per_track)

    }  else {
      max_displacement_per_track_collector <- c(max_displacement_per_track_collector, as.numeric(current_max_displacement_per_track))

    }


  }


  # put the coordinates vectors together
  answer <- data.frame(track_list, as.numeric(max_displacement_per_track_collector))

  colnames(answer) <- c("trackid", "maxdisplacement")

  return(answer)


}
