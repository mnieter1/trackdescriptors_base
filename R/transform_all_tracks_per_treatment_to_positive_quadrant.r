#' transform all tracks per treatment to positive quadrant
#'
#' @param tracktable_w_x_y_pos_n_treatment a table with x.position, y.position and treatment columns
#'
#' @return the input table with x and y coordinates per conditions transformed to positive quadrant
#' @export
#'
#' @examples
#' # first get the track cluster annotation
#' transform_all_tracks_per_treatment_to_positive_quadrant(
#'              tracktable_w_x_y_pos_n_treatment = testcase_tracklist)
transform_all_tracks_per_treatment_to_positive_quadrant <- function (tracktable_w_x_y_pos_n_treatment) {

  # for each condition make a transform coordinates
  conditions_are <- unique(tracktable_w_x_y_pos_n_treatment[ , "treatment"])
  nr_of_conditions <- length(conditions_are)

  for(condition_index in 1:nr_of_conditions) {
    # make subset
    pointer_list_to_condition <- which(as.character(tracktable_w_x_y_pos_n_treatment[ , "treatment"]) %in% as.character(conditions_are[condition_index]))

      treatments_x_positions <- tracktable_w_x_y_pos_n_treatment[pointer_list_to_condition, "x.position"]
      treatments_y_positions <- tracktable_w_x_y_pos_n_treatment[pointer_list_to_condition, "y.position"]

      min_of_x <- min(treatments_x_positions)
      min_of_y <- min(treatments_y_positions)

      treatments_x_positions <- treatments_x_positions - min_of_x
      treatments_y_positions <- treatments_y_positions - min_of_y

      tracktable_w_x_y_pos_n_treatment[pointer_list_to_condition , "x.position"] <-
        treatments_x_positions
      tracktable_w_x_y_pos_n_treatment[pointer_list_to_condition , "y.position"] <-
        treatments_y_positions
  }

  return(tracktable_w_x_y_pos_n_treatment)

}
