#' plot tracks
#'
#' @param coordinates_list the coordinates as 2D- or 3D-coordinate lists
#' @param track_id_list identifiers for separating the coordinates into tracks
#' @param condition_name the title of the plot, e.g stating the treatment condition
#' @param mark_start_end a boolean stating if start and endpoints should be marked
#' @param ylim classical ylim parameter to override the autolayout using the min and max values
#' @param xlim classical ylim parameter to override the autolayout using the min and max values
#'
#' @return a plot with the tracks in track space unicolor
#' @export
#'
#' @examples
#' # now plot the tracks
#'  plot_tracks(
#'      coordinates_list = DMSO_control_tracks[, c("x.position", "y.position")],
#'      track_id_list = DMSO_control_tracks[ , "trackid"],
#'      condition_name = "DMSO control")

plot_tracks <- function (coordinates_list,
                        track_id_list,
                        condition_name = "",
                        mark_start_end = TRUE, ylim = NULL, xlim = NULL) {

  # get the trackID list

  track_list <- unique(track_id_list)

  # get the number of tracks
  nr_of_tracks <- length(track_list)


  # find the min and max per dimension
  min_x <- min(coordinates_list[ , 1])
  min_y <- min(coordinates_list[ , 2])
  max_x <- max(coordinates_list[ , 1])
  max_y <- max(coordinates_list[ , 2])

  #min_min <- min(c(min_x, min_y))
  #max_max <- max(c(max_x, max_y))
  plotting_range_x <- c(min_x, max_x)
  plotting_range_y <- c(min_y, max_y)

  # override for zooming effect
  if(!is.null(xlim)){
    plotting_range_x <- xlim
  }
  if(!is.null(ylim)){
    plotting_range_y <- ylim
  }

  #plot all points
  plot(coordinates_list,
       col = "gray",
       pch = 19,
       main = condition_name,
       xlim = plotting_range_x,
       ylim = plotting_range_y
       )


  for(i in 1:nr_of_tracks){

    current_coordinate_index_list <- which(track_id_list %in% track_list[i])

    lines(x = coordinates_list[current_coordinate_index_list, 1],
          y = coordinates_list[current_coordinate_index_list, 2],
          col = "black",
          lwd = 3)

    # only mark if wanted
    if (mark_start_end == TRUE) {
    # mark starting point
    points(x = coordinates_list[current_coordinate_index_list[1], 1],
           y = coordinates_list[current_coordinate_index_list[1], 2],
           col = "red",
           pch = 4)
    # mark end point
    points(x = coordinates_list[current_coordinate_index_list[
      length(current_coordinate_index_list)], 1],
      y = coordinates_list[current_coordinate_index_list[
        length(current_coordinate_index_list)], 2],
      col = "green",
      pch = 17)
    }
  }
  mtext(paste("n tracks =", nr_of_tracks, sep=""), side = 3, line = 0, adj=1)
}
