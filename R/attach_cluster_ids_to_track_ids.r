#' attach cluster ids to a list of track ids
#'
#' @param track_ids a list with the track ids of the samples used before with hclust
#' @param hclust_result an hclust result
#' @param number_of_clusters the number of clusters derived by \code{dendextend::cut}
#'
#' @return a table with the track ids with attached cluster ids
#' @export
#'
#' @examples
#' track_ids_with_cluster_ids_attached <- attach_cluster_ids_to_track_ids(
#'  track_ids = testcase_tracklist[ , "trackid"],
#'  hclust_result = example_hclust_result_for_autocorrelation_descriptor,
#'  number_of_clusters = 6)
#'
attach_cluster_ids_to_track_ids <- function (track_ids, hclust_result, number_of_clusters = 6){

  list_with_track_ids_and_cluster_id <- extract_track_ids_and_attach_cluster_ids(hclust_result = hclust_result,
                                                                                 number_of_clusters = 6)

  # fix the one dimension error by specifying it to be dim length n x 1
  track_ids <- as.character(track_ids)
  dim(track_ids) <- c(length(track_ids), 1)
  colnames(track_ids) <- c("trackid")

  # make sure merge gets just characters to merge
  list_with_track_ids_and_cluster_id[] <- lapply(list_with_track_ids_and_cluster_id, as.character)

  track_ids_with_cluster_ids_annotated <- merge(track_ids, list_with_track_ids_and_cluster_id, by = "trackid", all.x = TRUE)


  return (as.data.frame(track_ids_with_cluster_ids_annotated))

  }
