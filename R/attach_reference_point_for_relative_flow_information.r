#' attach the reference coordinates of the flow closest query
#'
#'  adds coordinates of the flow reference point
#'  a.k.a. the closest reference point
#'
#' @param query_track_table a table with the query coordinates
#' @param ref_flow_track_table a table with reference points, serving the flow vectors
#' @param no_counter boolean to specify if counter index should be printed
#'
#' @return the "relative to flow" scalar product vector attached to query_track_table table
#' @export
#'
#' @examples
#' # none yet

attach_reference_point_for_relative_flow_information.r <- function(query_track_table,
                                           ref_flow_track_table,
                                           no_counter = FALSE){

  # create a collector
  collector <- NA

#   if(no_counter == F){
#     #initalize progress bar
#     pb <- txtProgressBar(...)
#   }
  # now for each positon with a displacement
  # find every reference from reference table
  for(query_coord_index in 1:(dim(query_track_table)[1])){

    if(no_counter == FALSE){
#       # update progress bar
#       setTxtProgressBar(pb, query_coord_index)
    # some feedback
    print(paste("current index is ", query_coord_index))
    }

    current_reference_point <- get_position_of_closest_reference_point(
                         ref_table = ref_flow_track_table,
                         query_coords = query_track_table[query_coord_index,
                                                                c("x.position",
                                                                  "y.position"),
                                                                drop = FALSE])

    if(query_coord_index == 1){
      collector <- current_reference_point
    }else{
      collector <- rbind(collector,
                         current_reference_point)
    }
  }
  rownames(collector) <- 1:dim(collector)[1]
  colnames(collector) <- c("x.position.of.reference",
                           "y.position.of.reference")
  # attach the collected classes to the query_table
  collector <- data.frame(query_track_table, collector)

#   if(no_counter == F){
#     close(pb)
#   }

  return(collector)
}
