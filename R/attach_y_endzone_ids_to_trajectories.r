#' attach X start zone ids to a coordinate list with track ids in "trackid" column
#'
#' @param trajectory_table a data.frame with track_ids in column "trackid"
#'  containing the track ids of the samples used before with hclust
#' @param nr_of_divisions the number of zones the dimension shall be divided
#' @param span  the global min max limit definition for the zones
#'
#' @return a table with the coordinates and track ids with attached x zone ids
#' @export
#'
#' @examples
#' track_ids_with_zone_ids_attached <- attach_y_endzone_ids_to_trajectories(
#'  trajectory_table = testcase_tracklist,
#'  nr_of_divisions = 10, span = c(0, 1500))
#'
attach_y_endzone_ids_to_trajectories <- function (trajectory_table, nr_of_divisions = 10, span = c(0, 1500)){


  zone_annotation_for_table <- get_endzone_index_per_track_id(
         one_D_coordinate_list = trajectory_table[ , "y.position"],
         track_ids = trajectory_table[ , "trackid"],
         nr_of_divisions = nr_of_divisions,
         span = span)

  zone_annotation_for_table <- data.frame(names(zone_annotation_for_table), zone_annotation_for_table)
  colnames(zone_annotation_for_table) <- c("trackid", "y_endzone")

  # make sure merge gets just characters as trackids to merge
  zone_annotation_for_table[ , "trackid"] <- as.character(zone_annotation_for_table[ , "trackid"])
  trajectory_table[ , "trackid"] <- as.character(trajectory_table[ , "trackid"])

  track_ids_with_zone_ids_annotated <- merge(trajectory_table, zone_annotation_for_table, by = "trackid", all.x = TRUE)

  return (as.data.frame(track_ids_with_zone_ids_annotated))

}
