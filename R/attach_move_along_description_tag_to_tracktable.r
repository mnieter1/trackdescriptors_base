#' attach move along class description to tracktable
#'
#' @param track_table a track table with coords and move.class
#' @param displacement_class_along_flow_columns the columns containing .move.class info (e.g. x and y)
#'
#' @return track table with attached movement class description tag
#' @export
#'
#' @examples
attach_move_along_description_tag_to_tracktable <- function(track_table, displacement_class_along_flow_columns = c("x.position.move.class", "y.position.move.class")){
  class_description_tag <- NA
  for(index in 1:dim(track_table)[1]){
    current_class_description_tag <- move_along_classed_as_description_tag(track_table[index, displacement_class_along_flow_columns])
    if(index == 1){
      class_description_tag <- current_class_description_tag
    }else{
      class_description_tag <- rbind(class_description_tag, current_class_description_tag)
    }

  }
  answer_table <- data.frame(track_table, class_description_tag)
  return(answer_table)
}
