
#' cluster membership distribution per treatment
#'
#' @param track_ids_w_treatment_class, define the treatment class list
#'        annotating the track IDs to be used
#' @param number_of_clusters, define number of different clusters
#' @param descriptors, the descriptor vector column wise
#'
#' @return matrix with the counts of a treatment found for each cluster as columns
#' @export
#'
#' @examples
#' test_output_for_6_clusters <- member_counts_per_cluster(
#'                  track_ids_w_treatment_class = unique(
#'                  testcase_tracklist[ , c("treatment", "trackid")]),
#'                  number_of_clusters = 6, descriptors = test_descriptor_with100divisions)
#'
member_counts_per_cluster <- function(track_ids_w_treatment_class, number_of_clusters, descriptors) {

  # prepare hierarchical cluster
  hc_result <- hclust(dist(t(descriptors),method="manhattan"), method = "ward.D") #  "ward.D", "ward.D2", "single", "complete", "average" (= UPGMA),
  # "mcquitty" (= WPGMA), "median" (= WPGMC) or "centroid" (= UPGMC).


  track_ids_with_cluster_ids_annotated <- extract_track_ids_and_attach_cluster_ids(
    hclust_result = hc_result,
    number_of_clusters = number_of_clusters)

  # get treatment list
  list_of_treatments <- unique(as.character(track_ids_w_treatment_class[, "treatment"]))
  # get cluster list
  list_of_clusters <- unique(as.character(track_ids_with_cluster_ids_annotated[, "clusterid"]))
  # merge the cluster information and treament annotation
  track_ids_with_cluster_ids_annotated_and_treatment <- merge(track_ids_with_cluster_ids_annotated, track_ids_w_treatment_class, by = "trackid", all.x = TRUE)


  collector <- NA


  # for each clusterID check the number of treatments in that cluster
  for(cluster_index in 1:length(list_of_clusters)){

      # get the subtable which hold sjust the entries in the current cluster
      subtable_for_current_cluster <- track_ids_with_cluster_ids_annotated_and_treatment[which(list_of_clusters[cluster_index] == track_ids_with_cluster_ids_annotated_and_treatment[ , "clusterid"]), ]

      # prepare collector for counts
      treatments_in_current_cluster <- rep(x = 0, length(list_of_treatments))


      # for each treatment
      for(treatment_index in 1:length(list_of_treatments)){


      # get count of the entries which are in the cluster with the current treatment

        treatments_in_current_cluster[treatment_index] <- sum(list_of_treatments[treatment_index] == subtable_for_current_cluster[ , "treatment"])

      }

      # if cluster_index == 1; the first cluster, build new collector table
      if (cluster_index == 1) {
        first_one <- treatments_in_current_cluster
      }else if (cluster_index == 2) {
        collector <- rbind(first_one, treatments_in_current_cluster)
      }else {
        collector <- rbind(collector, treatments_in_current_cluster)
      }

  }

  # attach correct names to the table row and col names
  rownames(collector) <- list_of_clusters
  colnames(collector) <- list_of_treatments

  return (as.data.frame(collector))
}
