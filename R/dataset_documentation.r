#' example sets of trajectories from four different conditions.
#'
#' A dataset containing the xy coordinates and track identifiers and
#' the conditions/treatment for a differential cell tracking experiment
#' from
#' 'WNT5A: a motility-promoting factor in Hodgkin lymphoma'
#'  Linke et al.
#' Oncogene. 2016 Jun 6. doi: 10.1038/onc.2016.183
#'
#' @format A data frame with 50400 rows and 4 variables:
#' \describe{
#'   \item{treatment}{condition, used in experiment}
#'   \item{x.position}{the x coordinate for the cell position}
#'   \item{y.position}{the y coordinate for the cell position}
#'   \item{trackid}{the unique identifier for a single cell}
#' }
#' @source \url{http://www.nature.com/onc/journal/vaop/ncurrent/full/onc2016183a.html}
"testcase_tracklist"

#' example DMSO set of trajectories from four different conditions.
#'
#' A dataset containing the xy coordinates and track identifiers and
#' the conditions/treatment for a differential cell tracking experiment
#' DMSO is the control population, without enhacer nor inhibitor
#' from
#' 'WNT5A: a motility-promoting factor in Hodgkin lymphoma'
#'  Linke et al.
#' Oncogene. 2016 Jun 6. doi: 10.1038/onc.2016.183
#'
#' @format A data frame with 50400 rows and 4 variables:
#' \describe{
#'   \item{treatment}{condition, used in experiment}
#'   \item{x.position}{the x coordinate for the cell position}
#'   \item{y.position}{the y coordinate for the cell position}
#'   \item{trackid}{the unique identifier for a single cell}
#' }
#' @source \url{http://www.nature.com/onc/journal/vaop/ncurrent/full/onc2016183a.html}
"DMSO_control_tracks"

#' example Wnt5a_enhancer_tracks set of trajectories from four different conditions.
#'
#' A dataset containing the xy coordinates and track identifiers and
#' the conditions/treatment for a differential cell tracking experiment
#' Wnt5a can be considered as an enhancer with prolonged tracks
#' from
#' 'WNT5A: a motility-promoting factor in Hodgkin lymphoma'
#'  Linke et al.
#' Oncogene. 2016 Jun 6. doi: 10.1038/onc.2016.183
#'
#' @format A data frame with 50400 rows and 4 variables:
#' \describe{
#'   \item{treatment}{condition, used in experiment}
#'   \item{x.position}{the x coordinate for the cell position}
#'   \item{y.position}{the y coordinate for the cell position}
#'   \item{trackid}{the unique identifier for a single cell}
#' }
#' @source \url{http://www.nature.com/onc/journal/vaop/ncurrent/full/onc2016183a.html}
"Wnt5a_enhancer_tracks"


#' example WntC59_inhibitor_tracks set of trajectories from four different conditions.
#'
#' A dataset containing the xy coordinates and track identifiers and
#' the conditions/treatment for a differential cell tracking experiment
#' WntC59 can be considered as an inhibitor with stalled tracks
#' from
#' 'WNT5A: a motility-promoting factor in Hodgkin lymphoma'
#'  Linke et al.
#' Oncogene. 2016 Jun 6. doi: 10.1038/onc.2016.183
#'
#' @format A data frame with 50400 rows and 4 variables:
#' \describe{
#'   \item{treatment}{condition, used in experiment}
#'   \item{x.position}{the x coordinate for the cell position}
#'   \item{y.position}{the y coordinate for the cell position}
#'   \item{trackid}{the unique identifier for a single cell}
#' }
#' @source \url{http://www.nature.com/onc/journal/vaop/ncurrent/full/onc2016183a.html}
"WntC59_inhibitor_tracks"

#' example mapping list for 'track ids vs treatment'
#'
#' A dataset containing the track identifiers and
#' the conditions/treatment for four differential cell tracking experiments
#' used to reattach/annotate the treatment back to the descriptor matrix
#' from
#' 'WNT5A: a motility-promoting factor in Hodgkin lymphoma'
#'  Linke et al.
#' Oncogene. 2016 Jun 6. doi: 10.1038/onc.2016.183
#'
#' @format A data frame with 50400 rows and 4 variables:
#' \describe{
#'   \item{treatment}{condition, used in experiment}
#'   \item{trackid}{the unique identifier for a single cell}
#' }
#' @source \url{http://www.nature.com/onc/journal/vaop/ncurrent/full/onc2016183a.html}
"mapping_list_tracks_treatments"


#' example descriptors derived from trajectories at four different conditions.
#'
#' A dataset containing the autocorrelation vectors derived tracks
#' for a differential cell tracking experiment
#' from
#' 'WNT5A: a motility-promoting factor in Hodgkin lymphoma'
#'  Linke et al.
#' Oncogene. 2016 Jun 6. doi: 10.1038/onc.2016.183
#'
#' @format A data frame with 100 rows and 700 variables:
#' \describe{
#'   \item{column name}{the unique track id}
#'   \item{row name}{autocorrellation bin position}
#' }
#' @source \url{http://www.nature.com/onc/journal/vaop/ncurrent/full/onc2016183a.html}
"test_descriptor_with100divisions"


#' example distribution of clusters derived from trajectories at four different conditions.
#'
#' A dataset containing the population distribution for the tracks classified by
#' the autocorrelation vector for the examplary differential cell tracking experiment.
#' The row names refer to the cluster id of the hclust result parsed with dendextend.
#' from
#' 'WNT5A: a motility-promoting factor in Hodgkin lymphoma'
#'  Linke et al.
#' Oncogene. 2016 Jun 6. doi: 10.1038/onc.2016.183
#'
#' @format A data frame with 6 rows and 4 variables:
#' \describe{
#'   \item{Box5}{counts of members for the condition Box5 - inhibitor}
#'   \item{DMSO}{counts of members for the condition DMSO - buffer control WT}
#'   \item{Wnt5a}{counts of members for the condition Wnt5a - enhancer}
#'   \item{Wnt-C59}{counts of members for the condition Wnt5-C59 - inhibitor}
#' }
#' @source \url{http://www.nature.com/onc/journal/vaop/ncurrent/full/onc2016183a.html}
"six_cluster_distribution_testcase_out"


#' example of an identifier list containing the representatives for the track population.
#'
#' An identifier list containing the representatives for the track population classified by
#' the autocorrelation vector for the examplary differential cell tracking experiment
#' using hclust for clustering and using dendextend to cut the tree into 15 subsets
#' as a last step the most average member of each cluster was derived and its identifier returned.
#' One can use this list:
#' e.g. to filter out the representative tracks for plotting the most diverse set.
#' from
#' 'WNT5A: a motility-promoting factor in Hodgkin lymphoma'
#'  Linke et al.
#' Oncogene. 2016 Jun 6. doi: 10.1038/onc.2016.183
#'
#' @format A data frame with 15 rows and 2 variables:
#' \describe{
#'   \item{representative.id}{the trajectory ID of the representative / most average member of the cluster}
#'   \item{group.id}{the cluster ID from applying \code{\link{dendextend}} cut to an \code{\link{hclust}} result}
#' }
#' @source \url{http://www.nature.com/onc/journal/vaop/ncurrent/full/onc2016183a.html}
"reference_results_for_representative_names_15_clusters"


#' example of an hclust result using descriptors for the track population.
#'
#' A regular \code{\link{hclust}} result derived by using the autocorrelation description of the example track population
#'
#' Call:
#' hclust(d = dist(t(test_descriptor_with100divisions), method = "manhattan"))
#' Cluster method   : complete
#' Distance         : manhattan
#' Number of objects: 700
#' from
#' 'WNT5A: a motility-promoting factor in Hodgkin lymphoma'
#'  Linke et al.
#' Oncogene. 2016 Jun 6. doi: 10.1038/onc.2016.183
#'
#' @format An hclust object:
#' \describe{
#'   \item{labels}{e.g contain the trajectory identifiers}
#'
#' }
#' @source \url{http://www.nature.com/onc/journal/vaop/ncurrent/full/onc2016183a.html}
"example_hclust_result_for_autocorrelation_descriptor"



#' mapping of tracks to clusters for four different conditions
#'
#' A mapping of all cluster IDs derived from dendextend cutting of an hclust.
#' The hclust result was previously derived by using the descriptors
#' of the full track set with foru conditions
#' from
#' 'WNT5A: a motility-promoting factor in Hodgkin lymphoma'
#'  Linke et al.
#' Oncogene. 2016 Jun 6. doi: 10.1038/onc.2016.183
#'
#' @format A data frame with 50400 rows and 2 variables:
#' \describe{
#'   \item{trackid}{the track IDs}
#'   \item{clusterid}{the cluster IDs from cutting an hclust}
#' }
#' @source \url{http://www.nature.com/onc/journal/vaop/ncurrent/full/onc2016183a.html}
"testcase_trackids_with_clusterids_for_6clusters"

#' DMSO set mapping of tracks to clusters
#'
#' A mapping of a subset cluster IDs derived from dendextend cutting of an hclust.
#' The hclust result was previously derived by using the descriptors
#' of the full track set with foru conditions
#' from
#' 'WNT5A: a motility-promoting factor in Hodgkin lymphoma'
#'  Linke et al.
#' Oncogene. 2016 Jun 6. doi: 10.1038/onc.2016.183
#'
#' @format A data frame with 18000 rows and 2 variables:
#' \describe{
#'   \item{trackid}{the track IDs}
#'   \item{clusterid}{the cluster IDs from cutting an hclust}
#' }
#' @source \url{http://www.nature.com/onc/journal/vaop/ncurrent/full/onc2016183a.html}
"DMSO_track_ids_with_cluster_ids_attached"

#' Wnt5a enhancer set mapping of tracks to clusters
#'
#' A mapping of a subset cluster IDs derived from dendextend cutting of an hclust.
#' The hclust result was previously derived by using the descriptors
#' of the full track set with foru conditions
#' from
#' 'WNT5A: a motility-promoting factor in Hodgkin lymphoma'
#'  Linke et al.
#' Oncogene. 2016 Jun 6. doi: 10.1038/onc.2016.183
#'
#' @format A data frame with 10800 rows and 2 variables:
#' \describe{
#'   \item{trackid}{the track IDs}
#'   \item{clusterid}{the cluster IDs from cutting an hclust}
#' }
#' @source \url{http://www.nature.com/onc/journal/vaop/ncurrent/full/onc2016183a.html}
"Wnt5a_enhancer_track_ids_with_cluster_ids_attached"

#' Wnt-C59 inhibitor set mapping of tracks to clusters
#'
#' A mapping of a subset cluster IDs derived from dendextend cutting of an hclust.
#' The hclust result was previously derived by using the descriptors
#' of the full track set with foru conditions
#' from
#' 'WNT5A: a motility-promoting factor in Hodgkin lymphoma'
#'  Linke et al.
#' Oncogene. 2016 Jun 6. doi: 10.1038/onc.2016.183
#'
#' @format A data frame with 10800 rows and 2 variables:
#' \describe{
#'   \item{trackid}{the track IDs}
#'   \item{clusterid}{the cluster IDs from cutting an hclust}
#' }
#' @source \url{http://www.nature.com/onc/journal/vaop/ncurrent/full/onc2016183a.html}
"WntC59_inhibitor_track_ids_with_cluster_ids_attached"



