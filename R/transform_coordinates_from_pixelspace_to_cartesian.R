#' transform coordinates from pixelspace to cartesian applying a cutoff
#'
#' @param a_3D_slice_stack a stack of slices from a 3D movie (e.g. 512 x 512 x 23);
#' a 3D array
#' @param video_width the width of the video, for instance in \eqn{\mu}m
#' @param video_height the heigth of the video, for instance in \eqn{\mu}m
#' @param video_depth the depth of the video, for instance in \eqn{\mu}m
#' @param cutoff the cutoff for defining the contour for the vessel
#'
#' @return an xyz coordinates table with vessel boundary positions
#' @export
#'
#' @examples
#' video_width <- 425.0961 # (512 pixels)
#' video_height <-  425.0961 # (512 pixels)
#' video_depth <- 150.0893 # (23 pixels)
#' # now create the table
#' transform_coordinates_from_pixelspace_to_cartesian(
#'  a_3D_slice_stack = example_video_snapshot_3d_array,
#'  cutoff = 100,
#'  video_width = video_width,
#'  video_height = video_height,
#'  video_depth = video_depth)
#'
transform_coordinates_from_pixelspace_to_cartesian <-
  function(a_3D_slice_stack,
           cutoff,
           video_width,
           video_height,
           video_depth) {
    # take the video resolution and scale accordingly x y and z
    video_resolution <- rep(NA, 3)
    video_resolution[1] <- dim(a_3D_slice_stack)[1]
    video_resolution[2] <- dim(a_3D_slice_stack)[2]
    video_resolution[3] <- dim(a_3D_slice_stack)[3]

    single_width_step <- video_width / video_resolution[1]
    single_height_step <- video_height / video_resolution[2]
    single_depth_step <- video_depth / video_resolution[3]
    # new faster render version
    # get the x y z coord list first then plot all at once
    xyz <- c(NA, NA, NA)

    # for each slice
    for (slice_index in 1:video_resolution[3]) {
      # extract the matrix at that slice level
      slice_matrix <- a_3D_slice_stack[, ,  slice_index]

      # remove some of the noise by applying the smoother
      slice_matrix <- mean_smooth_pixels_of_matrix(mean_smooth_pixels_of_matrix(slice_matrix))

      # remove some of the noise by applying the cutoff filter
      slice_matrix[(slice_matrix < cutoff)] <- NA

      # remove the inner parts of the vessel
      #slice_matrix <- remove_inner_pixels(slice_matrix)

      # binarize the matrix to hold NAs or specified intensity
      #slice_matrix <- binarize_pixels_na_or_value(slice_matrix,
      #                                            1000)

      for (rowindex in 1:video_resolution[1]) {
        for (colindex in 1:video_resolution[2]) {
          if (!is.na(slice_matrix[rowindex, colindex])) {
            # calculate position
            current_coordinate_at <-
              c((rowindex - 1) * single_width_step,
                (colindex - 1) * single_height_step,
                single_depth_step * (slice_index - 1)
              )
            # add it to the list of points
            xyz <- rbind(xyz, current_coordinate_at)

          }
        }
      }


    }
    # return the coordinate list of all slices all walls
    colnames(xyz) <- c("x.position", "y.position", "z.position")
    xyz <- xyz[complete.cases(xyz), ]
  return(xyz)

  }
