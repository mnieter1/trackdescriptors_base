#' radian to degree conversion
#'
#' @param rad a radian value
#'
#' @return a degree 0° to 360°
#' @export
#'
#' @examples
rad2deg <- function(rad) {(rad * 180) / (pi)}
#' degree to radian
#'
#' @param deg an angle in degree 0° to 360°
#'
#' @return a radian representation of the angle
#' @export
#'
#' @examples
deg2rad <- function(deg) {(deg * pi) / (180)}
