#' Determine vector direction overlay
#'
#' determines the overlay of the query vectors,
#' indicating parallel or antiparellel movement,
#' also taking a cutoff into account to determine movement theshold
#'
#' @param track_x_y coordinates x y and optional z, or even more
#' @param flow_x_y coordinates as above
#' @param cutoff_for_movement a cutoff definition
#' with minimal movement size to count as moved
#'
#' @return for each dimension the overlay determined by where_does_it_go
#' @export
#'
#' @examples
#' # none yet
go_with_the_flow <-
  function(track_x_y, flow_x_y, cutoff_for_movement) {
    # make variables
    flow_x_y_as_logical_direction <- rep(NA, length(track_x_y))
    track_x_y_as_logical_direction <- rep(NA, length(track_x_y))
    overlay_x_y_as_logical_direction_sum <- rep(NA, length(track_x_y))

    for (dimension in 1:(length(track_x_y))) {
      # make flow direction unit vectorized a.k.a. logical -1, 0, or 1
      flow_x_y_as_logical_direction[dimension] <-
        where_does_it_go(in_move = flow_x_y[dimension],
                         min_move_cutoff = cutoff_for_movement[dimension])

      # make flow direction unit vectorized a.k.a. logical -1, 0, or 1
      track_x_y_as_logical_direction[dimension] <-
        where_does_it_go(in_move = track_x_y[dimension],
                         min_move_cutoff = cutoff_for_movement[dimension])
    }

    # add the vector logicals to get direction overlap definition
    for (dimension in 1:(length(track_x_y))) {
      overlay_x_y_as_logical_direction_sum[dimension] <-
        track_x_y_as_logical_direction[dimension] +
        flow_x_y_as_logical_direction[dimension]
    }
    return(overlay_x_y_as_logical_direction_sum)
  }
