#' plot one kernel density plot of the endpoint displacement positions per condition
#'
#' the version with manually setting limits for the plotting space
#'
#' @param coordinates_list identifiers for separating the coordinates into tracks
#' @param track_id_list identifiers for separating the coordinates into tracks annotating the \code{coordinates_list}
#' @param conditions_list a list with the differents conditions annotating the \code{coordinates_list}
#' @param lims the global min max limit definition for the x and y zones (xl, xu, yl, yu)
#'
#' @return as many plots as there are different conditions found in the \code{conditions_list}
#' @export
#'
#' @examples
#' # plot one kde plot per treatment
#' plot_one_kde_centered_tracks_pic_per_treatment_use_just_endpoints_w_lims(
#'    coordinates_list = testcase_tracklist[ , c("x.position", "y.position")],
#'    track_id_list = testcase_tracklist[ , "trackid"],
#'    conditions_list = testcase_tracklist[ , "treatment"],
#'    lims = c(-300, 300, -300, 300))
#'
#'
#' # example with just the DMSO subset creates single plot
#' plot_one_kde_centered_tracks_pic_per_treatment_use_just_endpoints_w_lims(
#'      coordinates_list = DMSO_control_tracks[ , c("x.position", "y.position")],
#'      track_id_list = DMSO_control_tracks[ , "trackid"],
#'      conditions_list = DMSO_control_tracks[ , "treatment"],
#'      lims = c(-300, 300, -300, 300))

plot_one_kde_centered_tracks_pic_per_treatment_use_just_endpoints_w_lims <- function (coordinates_list, track_id_list, conditions_list, lims = c(-300, 300, -300, 300)) {


  # for each condition make a single plot
  conditions_are <- unique(conditions_list)
  nr_of_conditions <- length(conditions_are)

  for(condition_index in 1:nr_of_conditions) {
    # make subset
    pointer_list_to_condition <- which(as.character(conditions_list) %in% as.character(conditions_are[condition_index]))


    # make the plot
    plot_2d_kde_w_1d_hist_w_lims(
      coordinate_list_centered_at_origin = displacement_vector_per_track(coordinates_list = coordinates_list[pointer_list_to_condition, ],
      track_id_list = track_id_list[pointer_list_to_condition]),
      main_title = conditions_are[condition_index], lims = lims)

  }

}
