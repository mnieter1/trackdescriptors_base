#' attach Y start zone ids to a coordinate list with track ids in "trackid" column
#'
#' @param trajectory_table a data.frame with track_ids in column "trackid"
#'  containing the track ids of the samples used before with hclust
#' @param nr_of_divisions the number of zones the dimension shall be divided
#' @param span  the global min max limit definition for the zones
#'
#' @return a table with the coordinates and track ids with attached x zone ids
#' @export
#'
#' @examples
#' track_ids_with_zone_ids_attached <- attach_y_startzone_ids_to_trajectories(
#'  trajectory_table = testcase_tracklist,
#'  nr_of_divisions = 10, span = c(0, 1500))
#'
attach_y_startzone_ids_to_trajectories <- function (trajectory_table, nr_of_divisions = 10, span = c(0, 1500)){


  startzone_annotation_for_x <- get_startzone_index_per_track_id(
         one_D_coordinate_list = trajectory_table[ , "y.position"],
         track_ids = trajectory_table[ , "trackid"],
         nr_of_divisions = nr_of_divisions,
         span = span)

  startzone_annotation_for_x <- data.frame(names(startzone_annotation_for_x), startzone_annotation_for_x)
  colnames(startzone_annotation_for_x) <- c("trackid", "y_startzone")

  # make sure merge gets just characters as trackids to merge
  startzone_annotation_for_x[ , "trackid"] <- as.character(startzone_annotation_for_x[ , "trackid"])
  trajectory_table[ , "trackid"] <- as.character(trajectory_table[ , "trackid"])

  track_ids_with_zone_ids_annotated <- merge(trajectory_table, startzone_annotation_for_x, by = "trackid", all.x = TRUE)

  return (as.data.frame(track_ids_with_zone_ids_annotated))

}
