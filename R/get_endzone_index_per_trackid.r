#' take track coordinate_list and specify end zone index
#'
#' @param one_D_coordinate_list 1D-coordinate list,
#'  which should be used for determining start zone index
#' @param track_ids identifyers for separating the coordinate_list into tracks
#' @param nr_of_divisions how many zones/bins/divisions should be used
#' @param span  the global min max limit definition for the zones
#'
#' @return a data frame with the zone index and track identifyers
#' @export
#'
#' @examples
#' # define nr of bins
#' nr_of_divisions <- 100
#' # define a coordinate table
#' coordinate_list <- testcase_tracklist[ , c("x.position", "y.position")]
#' # define the trackID list to separate different tracks in the coordinate field
#' track_ids <- testcase_tracklist[ , "trackid"]
#' # call the autocorrelation vector maker
#' test_descriptor_with100divisions <- autocorrelation_vector_per_trackid(
#'                                    coordinate_list,
#'                                    track_ids,
#'                                    nr_of_divisions,
#'                                    distance_measure = "manhattan")
get_endzone_index_per_track_id <-
  function (one_D_coordinate_list, track_ids, nr_of_divisions, span = c(0, 1000)) {
    # to get the list of all tracks to loop
    unique_tracks <- unique(track_ids)
    nr_of_tracks <- length(unique_tracks)

    # get max of values
    max_value <- span[2]
    #print(max_value)

    # get min of values
    min_value <- span[1]
    #print(min_value)

    # initialize collector
    collector_of_zone_indices <- NA

    for (i in 1:nr_of_tracks) {
      current_coordinate_index_list <-
        which(track_ids %in% unique_tracks[i])

      # extract starting point
      start_x = one_D_coordinate_list[current_coordinate_index_list[
        length(current_coordinate_index_list)]]

      zone_index <-
        bin_drop_position(
          value = start_x,
          min_value = min_value,
          max_value = max_value,
          nr_of_divisions = nr_of_divisions
        )

      if (i == 1) {
        collector_of_zone_indices <- zone_index
      }else{
        collector_of_zone_indices <-
          c(collector_of_zone_indices, zone_index)
      }

    }

    names(collector_of_zone_indices) <- unique_tracks
    return (collector_of_zone_indices)
  }
