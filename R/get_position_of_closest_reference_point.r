#' get local vector of closest reference point
#'
#' @param ref_table a table with coords and local vectors (.move) defining next movement
#' @param query_coords as matrix, using drop=F when extracting to allow colnames for search in ref table
#'
#' @return the local reference vector closest to query if more than one their average
#' @export
#'
#' @examples
get_position_of_closest_reference_point <- function(ref_table, query_coords){

  # get just the coords from reference table
  just_the_ref_coords <- ref_table[ , colnames(query_coords), drop = FALSE]
  # join the coords from ref with the query coords
  joined_coord_list <- rbind(query_coords, just_the_ref_coords)
  # caluclate the distances; quick and dirty all to all even though we only need first to all others
  # appears to be to dirty in terms of memory :-)
  #dist_matrix_for_query_to_ref <- as.matrix(dist(x = joined_coord_list, method = "manhattan"))
  ## just one column is enough, already exclude reference to query
  #distvector <- dist_matrix_for_query_to_ref[2:dim(dist_matrix_for_query_to_ref)[1] , 1]

  distvector <- NA
  for(index in 1:dim(just_the_ref_coords)[1]){
    current_dist <- dist(joined_coord_list[c(1, (index+1)) , ], method = "manhattan")

    if(index == 1){
      distvector <- current_dist
    }else{
      distvector[index] <- current_dist

    }
  }
  # find the minimal distance value possible for query point
  current_min_distance <- min(distvector)
  # use min value to find the closest point(s)
  pointer_to_mins <- which(distvector == current_min_distance)
  # extract the local displacement vector coords ".move" columns
  # and make a mean before returning result, therefore no drop of dimensions allowed
  local_ref_position <- colMeans(
    ref_table[pointer_to_mins,
              paste(colnames(query_coords),
                    "",
                    sep = ""),
              drop = FALSE])
  #colnames(local_ref_position) <- paste(colnames(query_coords),
  #                                      ".of.reference",
  #                                      sep = "")
  return(local_ref_position)
}
