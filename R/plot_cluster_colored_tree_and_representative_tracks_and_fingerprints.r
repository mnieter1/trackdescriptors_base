#' plot the clustering results of a set of tracks with representatives for the
#' descriptor vectors specified
#'
#' @aliases hcluster representatives
#' @param track_coordinate_list 2D- or 3D-coordinate lists
#' @param track_id_list the name list corresponding to the coordinate list
#' @param description_vector_per_trackid a matrix containing vectors containing
#'   the descriptions of a track columnwise
#' @param number_of_clusters defines number of different clusters
#'
#' @return a plot with a colored dendrogram to show specified number of clusters
#'   and the representatives of these clusters as 2D coordinate plots and
#'   fingerprints of the corresponding descriptor vectors as barplots
#' @export
#' @importFrom magrittr %>%
#' @importFrom dendextend set
#'
#' @examples
#' plot_cluster_colored_tree_and_representative_tracks_and_fingerprints(
#'    track_coordinate_list = testcase_tracklist[, c("x.position", "y.position")],
#'    track_id_list = testcase_tracklist[, "trackid"],
#'    description_vector_per_trackid = test_descriptor_with100divisions,
#'    number_of_clusters = 9)
plot_cluster_colored_tree_and_representative_tracks_and_fingerprints <- function (
                                                            track_coordinate_list,
                                                            track_id_list,
                                                description_vector_per_trackid,
                                                            number_of_clusters){



# clustering start ------------------------------------------

#hclust of tracks
# prepare hierarchical cluster
hc = hclust(dist(t(as.matrix(description_vector_per_trackid)),method="manhattan"), method = "ward.D") #  "ward.D", "ward.D2", "single", "complete", "average" (= UPGMA),
# "mcquitty" (= WPGMA), "median" (= WPGMC) or "centroid" (= UPGMC).


index_list_for_ordered_depiction_as_with_dendrogram <- hc$order
label_list_for_ordered_depiction_as_with_dendrogram <- hc$labels



# ?????ß Hä wieso zwei
# to get the list of all tracks to loop
unique_track_ids <- colnames(autocorrelation_vector_per_trackid)
# if the above is changed to the orderered labels list it should order the plots below as well
unique_track_ids <- label_list_for_ordered_depiction_as_with_dendrogram[index_list_for_ordered_depiction_as_with_dendrogram]


# nr_of_miniplots <- length(unique_track_ids)
# use the number of clusters to estimate number of miniplots
nr_of_miniplots <- number_of_clusters

#make a long vector with just ones and the one with 2, 3,4 to the length of nr_of_miniplots+1
#firstcolumn <- rep(1, nr_of_miniplots)
#secondcolumn <- seq(from=2, to=(nr_of_miniplots+1), by=1)

defineTop <- 0.95
definebottom <- 0.05
rangefromtoptobottom <- defineTop - definebottom

#dendroplotscreen <- c(0, 0.25, definebottom, defineTop)# the first plot to the left side panel
dendroplotscreen <- c(0, 0.25, definebottom+0.05, defineTop-0.05)# the first plot to the left side panel


miniplotheight <- rangefromtoptobottom / nr_of_miniplots
currenttop <- defineTop
current_bottom <- currenttop - miniplotheight

offsetleftside <- 0.3
rightside <- offsetleftside + miniplotheight # to make it square boxes

current_subplot_screen <- c(offsetleftside, rightside, current_bottom, currenttop)


m <- NULL

for (i in 1 : nr_of_miniplots){
  if(i == 1){
    m <- rbind(dendroplotscreen, current_subplot_screen)
  }else{
    m <- rbind(m, current_subplot_screen)

  }
  currenttop <-  current_bottom
  current_bottom <- currenttop - miniplotheight
  current_subplot_screen <- c(offsetleftside, rightside, current_bottom, currenttop)

}
# add another column with fingerprints
currenttop <- defineTop
current_bottom <- currenttop - miniplotheight
offsetbetweenplotcolumns <- 0.05

offsetleftside <- offsetleftside + miniplotheight + offsetbetweenplotcolumns
rightside <- offsetleftside + miniplotheight*3 # fingerprints more extended

current_subplot_screen <- c(offsetleftside, rightside, current_bottom, currenttop)

for (i in 1 : nr_of_miniplots){


  m <- rbind(m, current_subplot_screen)


  currenttop <-  current_bottom
  current_bottom <- currenttop - miniplotheight
  current_subplot_screen <- c(offsetleftside, rightside, current_bottom, currenttop)

}

split.screen(m)

screen(1)
par(mar = c(0,0,2,0), oma = c(0,0,0,0))


hcd <- as.dendrogram(hc, hang = -1, labels = FALSE)


# extract the groups like in the dendrogram left is first, horzontal it's the lowerest
groups <- dendextend::cutree(hcd, k=nr_of_miniplots,
                             order_clusters_as_data = FALSE) # cut tree into nr_of_miniplots <-  clusters

#define color blind palette (http://www.cookbook-r.com/Graphs/Colors_(ggplot2)/)
# The palette with black:
#cbb_palette <- c("#000000", "#E69F00", "#56B4E9", "#009E73", "#F0E442", "#0072B2", "#D55E00", "#CC79A7")
# The palette with grey:
#cbb_palette <- c("#999999", "#E69F00", "#56B4E9", "#009E73", "#F0E442", "#0072B2", "#D55E00", "#CC79A7")
# colorfull color blind palette (http://dr-k-lo.blogspot.de/2013/07/a-color-blind-friendly-palette-for-r.html)
cbb_palette <- c("#009E73", "#e79f00", "#9ad0f3", "#0072B2", "#D55E00",
                "#CC79A7", "#F0E442", "#000000")

# To use for fills, add
# scale_fill_manual(values=cbPalette)

# To use for line and point colors, add
#scale_colour_manual(values=cbPalette)



hcd %>% set("labels_cex", 2) %>%
  set("labels_col", value = c(2:(number_of_clusters+1)), k=number_of_clusters) %>%
  #set("branches_k_col", value = c(2:(number_of_clusters+1)), k=number_of_clusters) %>%
  set("branches_k_col", value = cbb_palette, k=number_of_clusters) %>%

  # set("branches_lty", 3) %>% # dashed line

  set("branches_lwd", 4) %>%
  set("labels", NA) %>%

  plot(main = paste(number_of_clusters, " main classes"), horiz=T)


# representatives list ----------------------------------------------------

# getting the list of representative ids

results_for_representative_names_clusters <- representatives_id_per_cluster(
  hcclust_result = hc,
  description_vector_per_trackid =  description_vector_per_trackid,
  number_of_clusters = number_of_clusters)

# adding the scatterplots -----------------------------------------------------

# get max of values
max_value <- -Inf

# get min of values
min_value <- Inf


max_dist_x = -Inf
max_dist_y = -Inf
#max_distz = -Inf

unique_track_ids <- results_for_representative_names_clusters[, "representative.id"]

for(track_index in 1:length(unique_track_ids)){

  # get the tracks in the order defined as representatives
  # how to get the coordinate list filtered
  # subtable with just one track
  single_track <- track_coordinate_list[which(track_id_list %in% unique_track_ids[track_index]),]

  single_track_dist_matrix <- as.matrix(dist(single_track, method = "manhattan", diag = TRUE))

  x_range = range(single_track[ , 1])
  y_range = range(single_track[ , 2])
  #zrange = range(single_track$"Position Z")

  x_dist = x_range[2]-x_range[1]
  y_dist = y_range[2]-y_range[1]
  #zdist = zrange[2]-zrange[1]


  # get max of values
  if(max_dist_x < x_dist)
    max_dist_x <- x_dist

  if(max_dist_y < y_dist)
    max_dist_y <- y_dist

  #if(max_distz < zdist)
  #max_distz <- zdist


  # get min of values
  if(min_value > min(single_track_dist_matrix))
    min_value <- min(single_track_dist_matrix)


}

max_for_dim = max(c(max_dist_x,max_dist_y))

plotting_range <- c(0, max_for_dim)

#colors used to color the histogram relates to the coloring of the branches
streched_cbb_palette <- array(cbb_palette, dim=c(nr_of_miniplots,1))


# now plot the tracks one by one
#use the screens
screen_miniplot_index <- 2
# first pass was to get same dim scaling for all tracks so now plot them
for(track_index in length(unique_track_ids):1){

  # get the tracks in the order defined as representatives

  # subtable with just one track
  single_track <- track_coordinate_list[which(track_id_list %in% unique_track_ids[track_index]), ]

  # plot the track
  # switch to next plotting area
  screen(screen_miniplot_index)

  #transpose to min zeros
  track_min_x = min(single_track[ , 1])
  track_min_y = min(single_track[ , 2])
  #trackminz = min(single_track$"Position Z")

  single_track[ , 1] <- single_track[ , 1] - track_min_x
  single_track[ , 2] <- single_track[ , 2] - track_min_y
  #single_track$"Position Z" <- single_track$"Position Z" - trackminz


  par(mar = c(2,0,0,0))
  #plot(single_track, col="red", xlim=c(0,max_for_dim), ylim=c(0,max_for_dim))

  plot(single_track, col="gray", pch=19, ylim=plotting_range, xlim=plotting_range)

  lines(single_track, col=c(streched_cbb_palette[track_index]), lwd=3)

  # mark starting point
  points(x = single_track[1, 1],
         y = single_track[1, 2],
         col="red", pch=4)
  # mark end point
  points(x = single_track[length(single_track[ , 1]), 1],
         y = single_track[length(single_track[ , 1]), 2],
         col="green", pch=17)



  # switch to next plotting position index
  screen_miniplot_index <- screen_miniplot_index + 1

}


# fingerprints plots ----------------------------------------------------------

# how to get the fingerprint

# now plot the fingerprints one by one
#use the screens
screen_miniplot_index <- 2 + nr_of_miniplots

#colors used to color the histogram relates to the coloring of the branches
streched_cbb_palette <- array(cbb_palette, dim=c(nr_of_miniplots,1))

for(track_index in length(unique_track_ids):1){
  # switch to next plotting area
  screen(screen_miniplot_index)

  par(mar = c(2,0,0,0))

  # subtable with just one descriptor vector
  single_descriptor_vector <- description_vector_per_trackid[ , which(colnames(description_vector_per_trackid) %in% unique_track_ids[track_index])]

  # make the barplot
  barplot(t(single_descriptor_vector), col=c(streched_cbb_palette[track_index]))

  screen_miniplot_index <- screen_miniplot_index + 1

}


close.screen(all.screens = TRUE)

}
