#' get local vector to table
#'
#' @param table_w_coords a table containing x.position, y.position and trackid
#'
#' @return the table with attached local vectors
#' @export
#'
#' @examples
#' get_local_vector_to_table(table_w_coords = testcase_tracklist)
#'
get_local_vector_to_table <- function(table_w_coords){
 # find total number of points
  nr_of_points <- dim(table_w_coords)[1]
  # make first pointer index list
  first_index_list <- 1:(nr_of_points-1)
  # make first pointer index list
  second_index_list <- 2:(nr_of_points)

  collector_of_vectors <- NA

  # do not do this for last point / end point
  for(start_point_index in 1:nr_of_points-1){
    current_vector <- displacement_vector_per_track(
      coordinates_list = table_w_coords[c(first_index_list[start_point_index],
                                          second_index_list[start_point_index]),
                                        c("x.position", "y.position")],
      track_id_list = c(start_point_index, start_point_index))

        if(start_point_index == 1){
      collector_of_vectors <- current_vector
    }else{
      collector_of_vectors <- rbind(collector_of_vectors, current_vector)
    }

  }

  return(collector_of_vectors)
}
