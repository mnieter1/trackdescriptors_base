#' plot tracks as points in a existing plot - a.k.a. adding to plot
#'
#' @param coordinates_list the coordinates as 2D- or 3D-coordinate lists
#' @param track_id_list identifiers for separating the coordinates into tracks
#' @param mark_start_end a boolean stating if start and endpoints should be marked
#' @param set_name a tag for this data set
#' @param line_to_plot_tag in which outer line to plot the tag legend
#' @param color_for_positions the color for all points except ends
#'
#' @return a plot with the tracks in track space unicolor
#' @export
#'
#' @examples

plot_tracks_as_points_in_existing_plot <- function (coordinates_list,
                        track_id_list,
                        mark_start_end = TRUE, set_name = "", line_to_plot_tag = 1, color_for_positions = "gray") {

  # get the trackID list

  track_list <- unique(track_id_list)

  # get the number of tracks
  nr_of_tracks <- length(track_list)


  #plot all points
  points(x = coordinates_list[ , 1],
         y = coordinates_list[ , 2],
       col = color_for_positions,
       pch = 19
       )


  for(i in 1:nr_of_tracks){

    current_coordinate_index_list <- which(track_id_list %in% track_list[i])

    lines(x = coordinates_list[current_coordinate_index_list, 1],
          y = coordinates_list[current_coordinate_index_list, 2],
          col = "black",
          lwd = 3)

    # only mark if wanted
    if (mark_start_end == TRUE) {
    # mark starting point
    points(x = coordinates_list[current_coordinate_index_list[1], 1],
           y = coordinates_list[current_coordinate_index_list[1], 2],
           col = "red",
           pch = 4)
    # mark end point
    points(x = coordinates_list[current_coordinate_index_list[
      length(current_coordinate_index_list)], 1],
      y = coordinates_list[current_coordinate_index_list[
        length(current_coordinate_index_list)], 2],
      col = "green",
      pch = 17)
    }
  }
  mtext(paste(set_name, " n tracks =", nr_of_tracks, sep=""), side = 3, line = line_to_plot_tag, adj=1, col = color_for_positions)
}
