
#' extract track ids and attach cluster ids
#'
#' @param hclust_result the result of an hierarchical clustering with \code{\link{hclust}}
#' @param number_of_clusters the number of clusters to be extracted using \code{\link{dendextend}}
#'
#' @return a data frame with the track ids in one column and the corresponding cluster ids in the other
#' @export
#'
#' @examples
#' track_ids_with_cluster_ids_annotated <- extract_track_ids_and_attach_cluster_ids(
#'  hclust_result = example_hclust_result_for_autocorrelation_descriptor,
#'  number_of_clusters = 6)
extract_track_ids_and_attach_cluster_ids <- function (hclust_result, number_of_clusters = 6) {

  #hclust of tracks
  # prepare hierarchical cluster
  hc <- hclust_result


  index_list_for_ordered_depiction_as_with_dendrogram <- hclust_result$order
  label_list_for_ordered_depiction_as_with_dendrogram <- hclust_result$labels


  # to get the list of all tracks to loop
  # unique_tracks <- unique(track_ids)
  # if the above is changed to the orderered labels list it should order the plots below as well
  unique_tracks <- label_list_for_ordered_depiction_as_with_dendrogram[index_list_for_ordered_depiction_as_with_dendrogram]


  hcd <- as.dendrogram(hclust_result, hang = -1, labels = FALSE)


  # extract the groups like in the dendrogram left is first, horzontal it's the lowerest
  # cut tree into number_of_clusters <-  clusters
  groups <- dendextend::cutree(hcd, k=number_of_clusters, order_clusters_as_data = FALSE)
  # print(length(groups))

  cluster_members <- data.frame(as.character(names(groups)), as.numeric(groups))

  colnames(cluster_members) <- c("trackid", "clusterid")


  return (as.data.frame(cluster_members))
}
