
#' take coordinate list attach hclust cluster IDs
#'
#' @param data_frame_w_infos, a data frame containing trackids, coordinate columns and if wanted more information like treatment
#' @param list_of_coordinate_headers, a list with the headers of the coordinate columns to use
#' @param nr_of_clusters, the number of clusters/branches the hclust tree shall be cut into
#'
#' @return the data_frame_w_infos with attached clusterid column
#' @export
#'
#' @examples
#' # add cluster ids of an hclust result of the data derived for 6 clusters/branches
#' # to the sample coordinate test case
#' take_coordinate_list_attach_hclust_results(
#'  data_frame_w_infos = testcase_tracklist,
#'  list_of_coordinate_headers = c("x.position", "y.position"),
#'  nr_of_clusters = 6)
take_coordinate_list_attach_hclust_results <- function(data_frame_w_infos, list_of_coordinate_headers, nr_of_clusters){


coordinate_list <- data_frame_w_infos[ , list_of_coordinate_headers]

# auto cor vector returned
description_vector_per_trackid <- as.data.frame(autocorrelation_vector_per_trackid(
  coordinate_list = coordinate_list,
  track_ids = data_frame_w_infos[ , "trackid"],
  nr_of_divisions = 100,
  distance_measure = "manhattan"))

# clustern

hcclustresult <- hcluster_descriptors(
  description_vector_per_trackid = description_vector_per_trackid)

# attaching the cluster data to the data_frame_w_infos
track_ids_with_cluster_ids_attached <- attach_cluster_ids_to_trajectories(
  trajectory_table = data_frame_w_infos,
  hclust_result = hcclustresult,
  number_of_clusters = 6)


return(track_ids_with_cluster_ids_attached)
}
