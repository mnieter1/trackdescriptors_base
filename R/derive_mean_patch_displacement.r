#' derive mean patch displacement
#'
#' @param trajectory_table_w_zones a table with trackid, x.position, y.position, x_startzone, y_startzone
#' @param nr_of_x_zones how many zones along x axis
#' @param nr_of_y_zones how many zones along y axis
#' @param lims_to_use the limits for zoning e.g. c(xlower, xupper, ylower, yupper)
#'
#' @return return data frame with mean x and y displacement per patch and zone indices
#' @export
#'
#' @examples
#' # getting mean displacments per patch
derive_mean_patch_displacement <- function(trajectory_table_w_zones,
                                           nr_of_x_zones = 10,
                                           nr_of_y_zones = 10,
                                           lims_to_use = c(0, 1000, 0, 1000)) {
  collector_of_mean_patch_displacement <- NA

  # the interate over patches using x and y zone indices
  for (x_zone_index in 1:nr_of_x_zones) {
    for (y_zone_index in 1:nr_of_y_zones) {
      # get the tracks with that start index combination
      current_trackset <- trajectory_table_w_zones[which(
        trajectory_table_w_zones[, "x_startzone"] == x_zone_index &
          trajectory_table_w_zones[, "y_startzone"] == y_zone_index
      ),]

      # calculate start end displacement vector
      current_trackset_displacements <-
        displacement_vector_per_track(coordinates_list =
                                        current_trackset[, c("x.position", "y.position")],
                                      track_id_list = current_trackset[, "trackid"])

      # calculate the mean displacement vector for current zone
      current_trackset_displacements_means <-
        colMeans(current_trackset_displacements[, c("x.position", "y.position")],
                 na.rm = TRUE)


      if (x_zone_index == 1 & y_zone_index == 1) {
        collector_of_mean_patch_displacement <-
          c(x_zone_index, y_zone_index, current_trackset_displacements_means)
      } else {
        collector_of_mean_patch_displacement <-
          rbind(
            collector_of_mean_patch_displacement, c(
              x_zone_index, y_zone_index, current_trackset_displacements_means
            )
          )
      }

    }
  }
  return(collector_of_mean_patch_displacement)
}
