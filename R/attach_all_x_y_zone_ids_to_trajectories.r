#' attach all x and x start and end zone ids to a coordinate list with track ids in "trackid" column
#'
#' @param trajectory_table a data.frame with track_ids in column "trackid"
#'  containing the track ids of the samples used before with hclust
#' @param nr_of_divisions the number of zones the dimension shall be divided;
#'  one for x, one for y dimension
#' @param lims the box in which to create the zone indices (xl, xu, yl, xu)
#'
#' @return a table with the coordinates and track ids with attached x zone ids
#' @export
#'
#' @examples
#' tracks_with_zone_ids_attached <- attach_all_zone_ids_to_trajectories(
#'  trajectory_table = testcase_tracklist,
#'  nr_of_divisions = c(10, 10),
#'  lims = c(0, 1500, 0, 1500))
#'
attach_all_zone_ids_to_trajectories <- function (trajectory_table, nr_of_divisions = c(10, 10), lims = c(0, 1500, 0, 1500)){

  # first the x start zone
  trajectory_table <- attach_x_startzone_ids_to_trajectories(
    trajectory_table = trajectory_table,
    nr_of_divisions = nr_of_divisions[1],
    span = lims[1:2])
  # second the y start zone
  trajectory_table <- attach_y_startzone_ids_to_trajectories(
    trajectory_table = trajectory_table,
    nr_of_divisions = nr_of_divisions[2],
    span = lims[3:4])

  # third the x end zone
  trajectory_table <- attach_x_endzone_ids_to_trajectories(
    trajectory_table = trajectory_table,
    nr_of_divisions = nr_of_divisions[1],
    span = lims[1:2])
  # fourth the y end zone
  trajectory_table <- attach_y_endzone_ids_to_trajectories(
    trajectory_table = trajectory_table,
    nr_of_divisions = nr_of_divisions[2],
    span = lims[3:4])


  return (as.data.frame(trajectory_table))

}
