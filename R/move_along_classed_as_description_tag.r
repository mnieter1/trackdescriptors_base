#' move along classed as description tag
#'
#' returns colors to use for plotting as movement classes
#' "with flow" for along flow in at least one dimension
#' "against flow" against flow
#' "arrest" as stationary
#'
#' @param displacement_class_along_flow
#'
#' @return a color encoding relative movement class
#' @export
#'
#' @examples
#' move_along_classed_as_description_tag(c(-1,-1))
#' move_along_classed_as_description_tag(c(2,0))
#' move_along_classed_as_description_tag(c(0, 1))
move_along_classed_as_description_tag <- function(displacement_class_along_flow){
  # if there is a 2 it goes with the flow
  if(sum(abs(displacement_class_along_flow) == 2) > 0){
    color2use <- "with flow"
  }else if(sum(abs(displacement_class_along_flow) == 0) > 0){# its against the stream
    color2use <- "against flow"
  }else if(sum(abs(displacement_class_along_flow) == 1) == length(displacement_class_along_flow)){# if both are 1 , no movement
    color2use <- "arrest"
  }
  return(color2use)
}
