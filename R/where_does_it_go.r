#' which direction does it move
#'
#' @param in_move value of movement, can be positive or negative
#' @param min_move_cutoff define minimal distance to count as movement
#'
#' @return a -1, 0 or 1 encoding move to the "left", "nowhere" or the "right"
#' @export
#'
#' @examples
#' # a 1 for in_move = 2.5, min_move_cutoff = 0.5
#' where_does_it_go(in_move = 2.5, min_move_cutoff = 0.5)
#' # a 0 for in_move = 0.2, min_move_cutoff = 0.5
#' where_does_it_go(in_move = 0.2, min_move_cutoff = 0.5)
#' # a -1 for in_move = -2.5, min_move_cutoff = 0.5
#' where_does_it_go(in_move = -2.5, min_move_cutoff = 0.5)
where_does_it_go <-function(in_move, min_move_cutoff){
  out <- NA
  if(abs(in_move) < min_move_cutoff){
    out <- 0
  }else if(in_move > 0){
    out <- 1
  }else if(in_move < 0){
    out <- -1
  }
  return(out)
}
