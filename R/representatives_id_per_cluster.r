
#' return a list of representative names from hclust given a number of clusters
#'
#' @param hcclust_result, an hclust result obtained by using the \code{description_vector_per_trackid} as input
#' @param description_vector_per_trackid, a matrix containing vectors containing the descriptions of a track columnwise
#' @param number_of_clusters, define number of different clusters
#'
#' @return a list of representative names from hclust given a number of clusters, which can be used to filter the descriptors used e.g. for reduced mapping depictions of the data space
#' @export
#'
#' @examples
#' # take the reference descriptor and perform hierachical clusting
#' hc_dev = hclust(dist(t(test_descriptor_with100divisions), method = "manhattan"))
#' # calling the cluster representative function
#' test_results_for_representative_names_15_clusters <- representatives_id_per_cluster(
#' hcclust_result = hc_dev,
#' description_vector_per_trackid = test_descriptor_with100divisions,
#' number_of_clusters = 15)
representatives_id_per_cluster <- function (hcclust_result,
                                            description_vector_per_trackid,
                                            number_of_clusters = 6) {




  #hclust of tracks
  # prepare hierarchical cluster
  hc <- hcclust_result


  index_list_for_ordered_depiction_as_with_dendrogram <- hcclust_result$order
  label_list_for_ordered_depiction_as_with_dendrogram <- hcclust_result$labels




  # to get the list of all tracks to loop
  # unique_tracks <- unique(track_ids)
  # if the above is changed to the orderered labels list it should order the plots below as well
  unique_tracks <- label_list_for_ordered_depiction_as_with_dendrogram[index_list_for_ordered_depiction_as_with_dendrogram]


  # number_of_clusters <- length(unique_tracks)
  # use the number of clusters to estimate number of miniplots
  #number_of_clusters <- number_of_clusters


  #op <- par(mar = par("oma") + c(0,0,0,0))

  hcd <- as.dendrogram(hcclust_result, hang = -1, labels = FALSE)


  # extract the groups like in the dendrogram left is first, horzontal it's the lowerest
  groups <- dendextend::cutree(hcd, k=number_of_clusters, order_clusters_as_data = FALSE) # cut tree into number_of_clusters <-  clusters
  # print(length(groups))

  # print(paste("GesamtAnzahl der gefundenen Tracks im Datensatz", length(unique(knime.in[,"TrackID"]))))
  #cyle over clusters and summarize each treatment

  # creating the collector for the results
  answer_table_group_id_representatives_name <- cbind( c(number_of_clusters:1), rep("unkown", number_of_clusters))
  colnames(answer_table_group_id_representatives_name) <- c("group.id", "representative.id")

  for (group_index in number_of_clusters:1) {
    # print(paste("clusterindex is ", group_index))

    #get all the members names of the cluster
    current_cluster_members <-
      names(groups)[which(groups == group_index)]
    # print(paste("momentane Anzahl im Cluster ist ",length(current_cluster_members)))

    # check if there is only one member if so return the name; no dist calc
    # otherwise find the representative for that cluster
    name_of_representative_track <- "anonymous"
    if (length(current_cluster_members) == 1) {
      # it is a singelton
      name_of_representative_track <- current_cluster_members
    } else {
      # more than one candidate

      # extract the fingerprints for that cluster
      collection_of_cluster_fingerprints <-
        description_vector_per_trackid[,current_cluster_members]
      #dim(collection_of_cluster_fingerprints)

      cluster_dist_matrix <-
        dist(t(collection_of_cluster_fingerprints), upper = T)
      #dim(as.matrix(cluster_dist_matrix))
      all_distances_summed_for_a_cluster_member <-
        colSums(as.matrix(cluster_dist_matrix))

      name_of_representative_track <-
        names(all_distances_summed_for_a_cluster_member)[which(
          all_distances_summed_for_a_cluster_member == min(all_distances_summed_for_a_cluster_member)
        )]
      # check that there is only one representative, if more with equal dist sums just use the first
      if (length(name_of_representative_track) >= 2) {
        name_of_representative_track <- name_of_representative_track[1]
      }
    }


    # return the name_of_representative_track with group_index
    answer_table_group_id_representatives_name[group_index, "representative.id"] <- name_of_representative_track



  }

  return (as.data.frame(answer_table_group_id_representatives_name))
}
