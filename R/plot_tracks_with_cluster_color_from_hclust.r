#' plot tracks with cluster color from hclust depiction
#'
#' @param coordinates_list the coordinates as 2D- or 3D-coordinate lists
#' @param track_id_list identifiers for separating the coordinates into tracks
#' @param cluster_ids the cluster ids from hclust result used for coloring
#' @param condition_name the title of the plot, e.g stating the treatment condition
#' @param nr_of_clusters define number of different clusters
#' @param mark_start_end a boolean stating if start and endpoints should be marked
#'
#' @return a plot with the tracks in track space colored by cluster_id
#' @export
#'
#' @examples
#' # first get the track cluster annotation
#' DMSO_track_ids_with_cluster_ids_attached <- attach_cluster_ids_to_track_ids(
#'    track_ids = DMSO_control_tracks[ , "trackid"],
#'    hclust_result = example_hclust_result_for_autocorrelation_descriptor,
#'    number_of_clusters = 6)
#' # now plot the tracks
#'  plot_tracks_with_cluster_color_from_hclust(
#'      coordinates_list = DMSO_control_tracks[, c("x.position", "y.position")],
#'      track_id_list = DMSO_control_tracks[ , "trackid"],
#'      nr_of_clusters = 6,
#'      condition_name = "DMSO control",
#'      cluster_ids = DMSO_track_ids_with_cluster_ids_attached[ , "clusterid"])

plot_tracks_with_cluster_color_from_hclust <- function (coordinates_list,
                                                        track_id_list,
                                                        cluster_ids,
                                                        nr_of_clusters,
                                                        condition_name = "",
                                                        mark_start_end = TRUE) {
  # colorfull color blind palette
  #(http://dr-k-lo.blogspot.de/2013/07/a-color-blind-friendly-palette-for-r.html)
  cbbPalette <- c("#009E73", "#e79f00", "#9ad0f3", "#0072B2", "#D55E00",
                  "#CC79A7", "#F0E442", "#000000")

  nr_of_colors_to_use = nr_of_clusters

  streched_cbb_palette <- array(cbbPalette, dim=c(nr_of_colors_to_use, 1))
  color_palette_to_use = streched_cbb_palette

  #no turn here because color is like in barplot from left to right
  # invert because clusters come from bottom in turned dendrogram
  #color_palette_to_use = streched_cbb_palette[nr_of_colors_to_use:1]

  # get the trackID list

  track_list <- unique(track_id_list)

  # get the number of tracks
  nr_of_tracks <- length(track_list)


  # find the min and max per dimension
  min_x <- min(coordinates_list[ , 1])
  min_y <- min(coordinates_list[ , 2])
  max_x <- max(coordinates_list[ , 1])
  max_y <- max(coordinates_list[ , 2])

  max_max <- max(c(max_x, max_y))
  plotting_range <- c(0, max_max)


  #plot all points
  plot(coordinates_list,
       col = "gray",
       pch = 19,
       main = condition_name,
       ylim = plotting_range,
       xlim = plotting_range)

  for(i in 1:nr_of_tracks){

    current_coordinate_index_list <- which(track_id_list %in% track_list[i])

    lines(x = coordinates_list[current_coordinate_index_list, 1],
          y = coordinates_list[current_coordinate_index_list, 2],
          col = color_palette_to_use[
            as.numeric(as.character(cluster_ids[current_coordinate_index_list[1]]))],
          lwd = 3)

    # only mark if wanted
    if (mark_start_end == TRUE) {
    # mark starting point
    points(x = coordinates_list[current_coordinate_index_list[1], 1],
           y = coordinates_list[current_coordinate_index_list[1], 2],
           col = "red",
           pch = 4)
    # mark end point
    points(x = coordinates_list[current_coordinate_index_list[
      length(current_coordinate_index_list)], 1],
      y = coordinates_list[current_coordinate_index_list[
        length(current_coordinate_index_list)], 2],
      col = "green",
      pch = 17)
    }
  }
  mtext(paste("n tracks =", nr_of_tracks, sep=""), side = 3, line = 0, adj=1)
}
