#' batch load position files from imaris
#'
#' loads all imaris *_Position.csv files into one table which were located at target folder
#' 
#' sorted also by time per track
#' a file.chooser will open and ask you for a single example file in the location of the folder
#' if a path was specified using path variable it will use this information instead
#'
#' @param path the path to the folder or an example file within the top level folder;
#'  if not provided a file.choose window will pop up
#'
#' @return a table containing the positions per file, annotated with filename and path where it came from
#' @export
#'
#' @examples
#' path <- system.file("extdata", "some_experiments_cells_Position.csv", package = "trackdescriptors")
#' my_imaris_files_loaded_to_R <- batch_load_position_files(path)
batch_load_position_files <- function(path = NULL){

  where_is_it <- NULL
  # if a path was specified use it otherwise pop up the file chooser
  if (is.null(path)) {
    # choose the location of the files
    where_is_it <- file.choose()
    
  }else{
    # we just use the path given
    where_is_it <- path
  }
  # remember where you are now
  #where_do_I_start <- getwd()


  # extracts the folder of the example file
  path_to_files <- dirname(where_is_it)

  # get all path versions to the files of interest
  where_are_the_files <- list.files(path = path_to_files, recursive = TRUE, pattern = "_Position.csv")


  # filter out not fitting files using grep
  index_to_good_files <- grep(pattern = "_Track_Position", x = where_are_the_files, invert = TRUE)
  where_are_the_files <- where_are_the_files[index_to_good_files]


  collector_table <- NA

  for (index_of_files in 1:length(where_are_the_files)) {

    # find the local path part to go
    #current_folder <-  dirname(where_are_the_files[index_of_files])
    # combine with entry position definition path_to_files
    #current_folder <- paste(path_to_files, current_folder, sep = "/")
    # go there
    #setwd(dir = current_folder)

    # combine with local file path position definition with entry position path_to_files
    current_file <- paste(path_to_files,where_are_the_files[index_of_files], sep = "/")
    print(paste("current file to load is", current_file))
    # load table
    # use loader function for Position files return a table
    current_table <- load_an_imaris_position_file(current_file)

    # append table
    # if first create otherwise append
    if(index_of_files == 1){
      collector_table <- current_table
    }else{
      collector_table <- rbind(collector_table, current_table)
    }

  }

  # ensure to have unique trackid column
  collector_table$trackid <- paste(collector_table$TrackID, "_FROM_", collector_table$filename, sep = "")

  # fix colnames if necessary
  #Position X,Position Y,Position Z
  colnames(collector_table)[which(colnames(collector_table) == "Position.X")] <- "x.position"
  colnames(collector_table)[which(colnames(collector_table) == "Position.Y")] <- "y.position"
  colnames(collector_table)[which(colnames(collector_table) == "Position.Z")] <- "z.position"
  colnames(collector_table)[which(colnames(collector_table) == "Unit")] <- "unit"
  colnames(collector_table)[which(colnames(collector_table) == "Time")] <- "time"


  #sort by time
  #collector_table <- collector_table %>% dplyr::group_by(trackid) %>% dplyr::arrange(Time)
  collector_table <- collector_table[ order(collector_table$trackid, collector_table$time), ]

  # return to start folder
  #setwd(dir = where_do_I_start)
  return(collector_table)
}
