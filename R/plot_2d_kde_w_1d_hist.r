

#' plot a 2d kernel density plot with 1d side histograms
#'
#' @param coordinate_list_centered_at_origin the coordinate list,
#'  which track start positions must be centered at origin
#' @param main_title the title for the plot, mostly a treatment and or replication name
#'
#' @return a plot with a 2D kde2d and marginal 1D side histograms
#' @export
#'
#' @importFrom MASS kde2d
#' @importFrom RColorBrewer brewer.pal
#'
#' @examples
#' Wnt5a_centered_points <- translate_all_track_start_positions_to_origin(
#'  coordinates_list = testcase_tracklist_w_clusters_j_Wnt5a[ , c("x.position", "y.position")],
#'  track_id_list = testcase_tracklist_w_clusters_j_Wnt5a[ , "trackid"])
#' WntC59_centered_points <- translate_all_track_start_positions_to_origin(
#'  coordinates_list = testcase_tracklist_w_clusters_j_WntC59[ , c("x.position", "y.position")],
#'  track_id_list = testcase_tracklist_w_clusters_j_WntC59[ , "trackid"])
#' DMSO_centered_points <- translate_all_track_start_positions_to_origin(
#'  coordinates_list = testcase_tracklist_w_clusters_j_DMSO[ , c("x.position", "y.position")],
#'  track_id_list = testcase_tracklist_w_clusters_j_DMSO[ , "trackid"])
#' Box5_centered_points <- translate_all_track_start_positions_to_origin(
#' coordinates_list = testcase_tracklist_w_clusters_j_Box5[ , c("x.position", "y.position")],
#' track_id_list = testcase_tracklist_w_clusters_j_Box5[ , "trackid"])
#' # enhancer
#' plot_2d_kde_w_1d_hist(Wnt5a_centered_points, main_title = "Wnt5a - enhancer")
#' # DMSO control
#' plot_2d_kde_w_1d_hist(DMSO_centered_points, main_title = "DMSO - control")
#' # inhibitor Box5
#' plot_2d_kde_w_1d_hist(Box5_centered_points, main_title = "Box5 - inhibitor")
#' # inhibitor Wnt-C59
#' plot_2d_kde_w_1d_hist(WntC59_centered_points, main_title = "Wnt-C59 - inhibitor")

plot_2d_kde_w_1d_hist <-
  function (coordinate_list_centered_at_origin, main_title = "") {
    # Color housekeeping
    #library(RColorBrewer)
    rf <- colorRampPalette(rev(RColorBrewer::brewer.pal(11,'Spectral')))
    r <- rf(32)



    h1_coordinate_list_centered_at_origin <-
      hist(coordinate_list_centered_at_origin[, "x.position"],
           breaks = 50,
           plot = F)
    h2_coordinate_list_centered_at_origin <-
      hist(coordinate_list_centered_at_origin[, "y.position"],
           breaks = 50,
           plot = F)
    top <-
      max(h1_coordinate_list_centered_at_origin$counts, h2_coordinate_list_centered_at_origin$counts)
    k_coordinate_list_centered_at_origin <-
      kde2d(coordinate_list_centered_at_origin[, "x.position"], coordinate_list_centered_at_origin[, "y.position"], n =
              50)

    # margins
    oldpar <- par()
    par(mar = c(3,3,1,1))
    layout(matrix(c(2,0,1,3),2,2,byrow = T),c(3,1), c(1,3))
    image(k_coordinate_list_centered_at_origin, col = r) #plot the image
    # mark origin
    abline(h=0, col="black", lwd=3, lty=3)
    abline(v=0, col="black", lwd=3, lty=3)

    par(mar = c(0,2,1,0))
    barplot(
      h1_coordinate_list_centered_at_origin$counts, axes = F, ylim = c(0, top), space = 0, col =
        'red'
    )
    par(mar = c(2,0,0.5,1))
    barplot(
      h2_coordinate_list_centered_at_origin$counts, axes = F, xlim = c(0, top), space = 0, col =
        'red', horiz = T

    )
    # the title if wanted
    par(oma=c(0,0,2,0))
    title(main = main_title,outer=T)

    par(oldpar)

  }
