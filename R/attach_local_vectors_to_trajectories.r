#' attach local vectors to a coordinate list
#'
#' @param trajectory_table a data.frame with track_ids in column "trackid"
#'  containing the AUSFORMULIEREN
#' @return a table with the coordinates and track ids with next local x y displacements attached
#' @export
#'
#' @examples
#' track_ids_with_local_vectors_attached <- attach_local_vectors_to_trajectories(
#'  trajectory_table = testcase_tracklist)
#'
attach_local_vectors_to_trajectories <- function (trajectory_table){

  # to get the list of all tracks to loop
  unique_tracks <- unique(trajectory_table[ , "trackid"])
  nr_of_tracks <- length(unique_tracks)

  # initialize collector
  collector_of_local_vector_annotations <- NA

  for (i in 1:nr_of_tracks) {
    current_coordinate_index_list <-
      which(trajectory_table[ , "trackid"] %in% unique_tracks[i])

    # pack the track into the local vector annotator
    current_local_vector_list <- get_local_vector_to_table(table_w_coords =
                                                             trajectory_table[
                                                               current_coordinate_index_list ,
                                                               c("x.position", "y.position")])
    # add empty entry for last point of track with index depending on the size of the track
    current_local_vector_list <- rbind(current_local_vector_list,
                                       c((dim(current_local_vector_list)[1]+1), NA, NA))

    # init the collector if first entry
    if (i == 1) {
      collector_of_local_vector_annotations <- current_local_vector_list
    }else{
      collector_of_local_vector_annotations <-
        rbind(collector_of_local_vector_annotations, current_local_vector_list)
    }

  }

  colnames(collector_of_local_vector_annotations) <- c("position.index",
                                                       "x.position.move",
                                                       "y.position.move")
  # join together the table with the vectors
  trajectory_table_w_local_vector_annotations <- data.frame(trajectory_table,
                                                            collector_of_local_vector_annotations)
  return (trajectory_table_w_local_vector_annotations)

}
