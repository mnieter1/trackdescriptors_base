#' attach the scalar product vector to relative flow reference
#'
#'  adds for each dimension the value of the scalar product for each dimension
#'  relative to the length of the reference flow vector at that position
#'
#' @param query_track_table a table with the query coordinates
#' @param ref_flow_track_table a table with reference points, serving the flow vectors
#' @param cutoff_for_movement a cutoff for each dimension to be a movement
#' @param no_counter boolean to specify if counter index should be printed
#'
#' @return the "relative to flow" scalar product vector attached to query_track_table table
#' @export
#'
#' @examples
#' # none yet

attach_relative_flow_direction_as_scalar_product_information <- function(query_track_table,
                                           ref_flow_track_table,
                                           cutoff_for_movement, no_counter = FALSE){
  # attach the local displacements to each table
  query_track_table <- attach_local_vectors_to_trajectories(trajectory_table =
                                                              query_track_table)
  ref_flow_track_table <- attach_local_vectors_to_trajectories(trajectory_table =
                                                                 ref_flow_track_table)
  # filter the tables to contain just the ones with local displacement information
  # using complete.cases
  query_track_table_w_next_displacements <-
    query_track_table[complete.cases(query_track_table),]

  print(paste("current dim of query_track_table_w_next_displacements",
              dim(query_track_table_w_next_displacements)[1]))

  ref_flow_track_table_w_next_displacements <-
    ref_flow_track_table[complete.cases(ref_flow_track_table),]

  # create a collector
  collector <- NA

#   if(no_counter == F){
#     #initalize progress bar
#     pb <- txtProgressBar(...)
#   }
  # now for each positon with a displacement
  # find every reference from reference table
  for(query_coord_index in 1:(dim(query_track_table_w_next_displacements)[1])){

    if(no_counter == F){
#       # update progress bar
#       setTxtProgressBar(pb, query_coord_index)
    # some feedback
    print(paste("current index is ", query_coord_index))
    }

    current_flow_logic_answer <-
      floating_along_scalar_products(track_x_y =
                         query_track_table_w_next_displacements[query_coord_index,
                                                                c("x.position.move",
                                                                  "y.position.move")],
                       flow_x_y = get_local_vector_of_closest_reference_point(
                         ref_table = ref_flow_track_table_w_next_displacements,
                         query_coords = query_track_table_w_next_displacements[query_coord_index,
                                                                c("x.position",
                                                                  "y.position"),
                                                                drop = FALSE]))

    if(query_coord_index == 1){
      collector <- current_flow_logic_answer
    }else{
      collector <- rbind(collector,
                         current_flow_logic_answer)
    }
  }
  rownames(collector) <- 1:dim(collector)[1]
  colnames(collector) <- paste(c("x.position.move",
                                 "y.position.move"), ".scalar", sep = "")
  # attach the collected classes to the query_table
  collector <- data.frame(query_track_table_w_next_displacements, collector)

#   if(no_counter == F){
#     close(pb)
#   }

  return(collector)
}
