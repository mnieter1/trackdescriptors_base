#' plot as density pathlength per treatment
#'
#' @param table_w_treatment_trackid_pathlength a table
#'  containing a min of three columns:
#'         "treatment", "trackid" and "pathlength"
#'
#' @return a ggplot with the maxdisplacement as density histograms per treatment
#' @export
#' @import ggplot2
#'
#' @examples
#' traveled_distances_for_testcase <- traveled_distance_per_track(
#'  coordinates_list = testcase_tracklist[ , c("x.position", "y.position")],
#'  track_id_list = testcase_tracklist[ , "trackid"])
#'
#'  tcw_pathlength <- merge(unique(
#'  testcase_tracklist[ , c("treatment", "trackid")]),
#'                        traveled_distances_for_testcase,
#'                        by = "trackid",
#'                        all = TRUE)
#'
#'  plot_as_density_path_lengths_of_track_per_treatment(tcw_pathlength)
plot_as_density_path_lengths_of_track_per_treatment <- function(
  table_w_treatment_trackid_pathlength){


  ggplot(table_w_treatment_trackid_pathlength,
         aes_string(x = "pathlength",
                    group = "treatment",
                    fill = "as.factor(treatment)")) +
    geom_density(position = "identity", alpha=0.5) +
    scale_fill_discrete(name = "treatment") +
    theme_bw() +
    xlab("values") + ggtitle("traveled distances")

}
