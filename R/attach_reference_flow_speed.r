#' attach the speed of the relative flow reference
#'
#' @param query_track_table a table with the query coordinates
#' @param ref_flow_track_table a table with reference points, serving the flow vectors
#' @param cutoff_for_movement a cutoff for each dimension to be a movement
#' @param no_counter boolean to specify if counter index should be printed
#'
#' @return the table with speed of flow reference attached to query_track_table table
#' @export
#'
#' @examples
#' # none yet

attach_reference_flow_speed <- function(query_track_table,
                                        ref_flow_track_table,
                                        cutoff_for_movement,
                                        no_counter = FALSE){
  # attach the local displacements to reference table, not required with query
  #query_track_table <- attach_local_vectors_to_trajectories(trajectory_table =
  #                                                            query_track_table)
  ref_flow_track_table <- attach_local_vectors_to_trajectories(trajectory_table =
                                                                 ref_flow_track_table)
  # filter the tables to contain just the ones with local displacement information
  # using complete.cases
  query_track_table_w_next_displacements <-
    query_track_table[complete.cases(query_track_table),]

  print(paste("current length of query_track_table_w_next_displacements",
              dim(query_track_table_w_next_displacements)[1]))

  ref_flow_track_table_w_next_displacements <-
    ref_flow_track_table[complete.cases(ref_flow_track_table),]

  # create a collector
  collector <- NA

  #   if(no_counter == F){
  #     #initalize progress bar
  #     pb <- txtProgressBar(...)
  #   }
  # now for each positon with a displacement
  # find every reference from reference table
  for(query_coord_index in 1:(dim(query_track_table_w_next_displacements)[1])){

    if(no_counter == F){
      #       # update progress bar
      #       setTxtProgressBar(pb, query_coord_index)
      # some feedback
      print(paste("current index is ", query_coord_index))
    }

    # get the flows local vector
    flow_x_y <- get_local_vector_of_closest_reference_point(
                             ref_table = ref_flow_track_table_w_next_displacements,
                             query_coords = query_track_table_w_next_displacements[query_coord_index,
                                                                                   c("x.position",
                                                                                     "y.position"),
                                                                                   drop = FALSE])
    # calculate the speed of track
    # calculate the local speed based on vector
    current_flow_speed_answer <- sqrt(flow_x_y[1]*flow_x_y[1] +
                                        flow_x_y[2]*flow_x_y[2])

    if(query_coord_index == 1){
      collector <- current_flow_speed_answer
    }else{
      collector <- rbind(collector,
                         current_flow_speed_answer)
    }
  }
  rownames(collector) <- 1:dim(collector)[1]
  colnames(collector) <- c("flow.track.speed")
  # attach the collected classes to the query_table
  collector <- data.frame(query_track_table_w_next_displacements, flow.track.speed =  collector)

  #   if(no_counter == F){
  #     close(pb)
  #   }

  return(collector)
}
