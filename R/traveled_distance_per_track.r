#' per track traveled distance aka path length
#'
#' @param coordinates_list the coordinates as 2D- or 3D-coordinate lists
#' @param track_id_list identifiers for separating the coordinates into tracks
#'
#' @return a travel distance list with total distance of trajectory,
#'        one per track specifying path length;
#'        returned columns are trackid and pathlength
#' @export
#'
#' @examples
#' testcase_path_length <- traveled_distance_per_track(
#'  coordinates_list = testcase_tracklist[ , c("x.position", "y.position")],
#'  track_id_list = testcase_tracklist[ , "trackid"])
traveled_distance_per_track <-
  function (coordinates_list, track_id_list) {
    # get the trackID list
    track_list <- unique(track_id_list)

    # get the number of tracks
    nr_of_tracks <- length(track_list)


    # collect path lenth per track
    traveled_distance_per_track_collector <- NA


    for (i in 1:nr_of_tracks) {
      current_coordinate_index_list <-
        which(track_id_list %in% track_list[i])


      # check if only one point then do not do the dist
      if (length(current_coordinate_index_list) != 1) {
        # get the distance matrix
        current_distmatrix <-
          as.matrix(dist(coordinates_list[current_coordinate_index_list,],
                         method = "euclidian"))

        # for each distance create the pointer indexes
        first_index_for_dist_matrix <- 1:(dim(current_distmatrix)[1] - 1)
        second_index_for_dist_matrix <- 2:(dim(current_distmatrix)[1])

        current_total_traveled_distance_per_track <- 0

        for (stepindex in 1:(dim(current_distmatrix)[1] - 1)) {
          current_total_traveled_distance_per_track <- current_total_traveled_distance_per_track + current_distmatrix[first_index_for_dist_matrix[stepindex], second_index_for_dist_matrix[stepindex]]
        }

      }else{
        # set distance to NA
        current_total_traveled_distance_per_track <- NA
      }

      # if first
      if (i == 1) {
        traveled_distance_per_track_collector <-
          as.numeric(current_total_traveled_distance_per_track)

      }  else {
        traveled_distance_per_track_collector <-
          c(
            traveled_distance_per_track_collector, as.numeric(current_total_traveled_distance_per_track)
          )

      }


    }


    # put the coordinates vectors together
    answer <-
      data.frame(track_list, as.numeric(traveled_distance_per_track_collector))

    colnames(answer) <- c("trackid", "pathlength")

    return(answer)


  }
