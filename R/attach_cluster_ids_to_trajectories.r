#' attach cluster ids to a coordinate list with track ids in "trackid" column
#'
#' @param trajectory_table a data.frame with track_ids in column "trackid"
#'  containing the track ids of the samples used before with hclust
#' @param hclust_result an hclust result
#' @param number_of_clusters the number of clusters derived by \code{dendextend::cut}
#'
#' @return a table with the coordinates and track ids with attached cluster ids
#' @export
#'
#' @examples
#' track_ids_with_cluster_ids_attached <- attach_cluster_ids_to_trajectories(
#'  trajectory_table = testcase_tracklist,
#'  hclust_result = example_hclust_result_for_autocorrelation_descriptor,
#'  number_of_clusters = 6)
#'
attach_cluster_ids_to_trajectories <- function (trajectory_table, hclust_result, number_of_clusters = 6){

  list_with_track_ids_and_cluster_id <- extract_track_ids_and_attach_cluster_ids(hclust_result = hclust_result,
                                                                                 number_of_clusters = number_of_clusters)

  # make sure merge gets just characters as trackids to merge
  list_with_track_ids_and_cluster_id[ , "trackid"] <- as.character(list_with_track_ids_and_cluster_id[ , "trackid"])
  trajectory_table[ , "trackid"] <- as.character(trajectory_table[ , "trackid"])

  track_ids_with_cluster_ids_annotated <- merge(trajectory_table, list_with_track_ids_and_cluster_id, by = "trackid", all.x = TRUE)

  return (as.data.frame(track_ids_with_cluster_ids_annotated))

}
