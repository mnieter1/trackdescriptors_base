#' plot the tracks upon 2D-background
#'
#' @param coordinates_list the coordinates as 2D- or 3D-coordinate lists
#' @param track_id_list identifiers for separating the coordinates into tracks
#' @param condition_name the title of the plot, e.g stating the treatment condition
#' @param mark_start_end T or F if the start and endpoints
#'        of the tracks should be marked in plot
#' @param slice_matrix the matrix containing the video intensities as 2D representation
#' @param video_width the width of the video in \eqn{\mu}m
#'        (the same as the coordinates from the tracks)
#' @param video_height the width of the video in \eqn{\mu}m
#'        (the same as the coordinates from the tracks)
#'
#' @return a plot with the tracks in track space colored by cluster_id
#' @export
#'
#' @examples
#' # plot the tracks upon 2D-background
#' plot_tracks_with_video2d_proj_as_bg(
#'  coordinates_list = example_tracks_for_depiction_w_bg[ , c("x.position", "y.position")],
#'  track_id_list = example_tracks_for_depiction_w_bg[ , "trackid"],
#'  condition_name = "example of 2D tracks and background overlay",
#'  mark_start_end = TRUE,
#'  video_width = 425.0961,
#'  video_height = 425.0961,
#'  slice_matrix = t(example_video_blood_vessel_as_2d_representation_picture))

plot_tracks_with_video2d_proj_as_bg <- function (coordinates_list, track_id_list, condition_name = "", mark_start_end = TRUE, slice_matrix, video_width = 425.0961, video_height =  425.0961) {
  # the video specifies the screen size not the tracks anymore
  #plotting_range <- c(0, video_width)
  # plotting the background derived from video
  video_resolution <- dim(slice_matrix)[1]
  #video_resolution[1] <- 512
  video_resolution[2] <- dim(slice_matrix)[2]

  single_width_step <- video_width / video_resolution[1]
  single_height_step <- video_height / video_resolution[2]

  max_intensity <- max(slice_matrix, na.rm = T)

  plot(0, type="n", xlim = c(0, video_width), ylim = c(0, video_height), xlab = expression(paste(mu, "m position", sep = "")),  ylab = expression(paste(mu, "m position", sep = "")), main = condition_name)

  for(rowindex in 1: video_resolution[1]){
    for(colindex in 1: video_resolution[2]){
      if(!is.na(slice_matrix[rowindex, colindex])){

        points(x = (rowindex -1) * single_width_step,
               y = (colindex -1) * single_height_step,
               col = adjustcolor("black", slice_matrix[rowindex, colindex]/max_intensity), pch=20)
      }
    }
  }

  #no turn here because color is like in barplot from left to right
  # invert because clusters come from bottom in turned dendrogram
  #color_palette_to_use = streched_cbb_palette[nr_of_colors_to_use:1]

  # get the trackID list

  track_list <- unique(track_id_list)

  # get the number of tracks
  nr_of_tracks <- length(track_list)

  #plot all points
  points(coordinates_list, col = "gray", pch = 19)

  for(i in 1:nr_of_tracks){

    current_coordinate_index_list <- which(track_id_list %in% track_list[i])
    #lines(knime.in$"x-Koordinate"[current_coordinate_index_list],knime.in$"y-Koordinate"[current_coordinate_index_list])

    #print(paste(color_palette_to_use[as.numeric(as.character(cluster_ids[current_coordinate_index_list[1]]))], cluster_ids[current_coordinate_index_list[1]]))

    lines(x = coordinates_list[current_coordinate_index_list, 1],
          y = coordinates_list[current_coordinate_index_list, 2],
          col = "blue",
          lwd = 3)

    # only mark if wanted
    if (mark_start_end == TRUE) {
    # mark starting point
    points(x = coordinates_list[current_coordinate_index_list[1], 1], y = coordinates_list[current_coordinate_index_list[1], 2], col = "red", pch = 4)
    # mark end point
    points(x = coordinates_list[current_coordinate_index_list[length(current_coordinate_index_list)], 1], y = coordinates_list[current_coordinate_index_list[length(current_coordinate_index_list)], 2], col = "green", pch = 17)
    }
  }
}
