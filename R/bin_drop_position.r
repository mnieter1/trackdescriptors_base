#' calculate bin position using min and max and number of bins
#' @aliases binning discretize
#' @param value The value to be placed in binning container vector of length \code{nr_of_divisions}
#' @param min_value The defined miniumum of the values to be represented by the binning vector
#' @param max_value The defined maximum of the values to be represented by the binning vector
#' @param nr_of_divisions The number of bins stretching over the span between \code{min_value} and \code{max_value}
#'
#' @return The index of the bin where the value in question should be 'placed' (R style: starting with 1, not 0)
#' @export
#'
#' @examples
#' bin_drop_position(15, 0, 100, 10)
#' currentValue <- 27
#' minborder <- 0
#' maxborder <- 100
#' current_nr_of_divisions <- 10
#' bin_drop_position(value=currentValue, min_value=minborder,
#'                  max_value=maxborder, nr_of_divisions=current_nr_of_divisions)
bin_drop_position <- function(value, min_value, max_value, nr_of_divisions) {
  # if value is out of specified range, return NA
  if (value < min_value | value > max_value) {
    return (NA)
  } else if (value == min_value) {
    # return first bin
    return (1)
  } else if (value == max_value) {
    # return last bin
    return (nr_of_divisions)
  } else {

    binpos <- floor((value-min_value)/(max_value-min_value)*nr_of_divisions)
    # adding the R offset at last
    return (binpos+1)
  }
}
