#' plot as density y_start_end distance of tracks per treatment
#'
#' @param table_w_treatment_trackid_xy_displacements
#'  the table containing a min of three columns:
#'         "treatment", "trackid" and "maxdisplacement"
#'
#' @return a ggplot with the x_start_end distance of tracks as density histograms per treatment
#' @export
#' @import ggplot2
#'
#' @examples
#' xy_displacements_for_testcase <- displacement_vector_per_track(
#'  coordinates_list = testcase_tracklist[ , c("x.position", "y.position")],
#'  track_id_list = testcase_tracklist[ , "trackid"])
#'
#'  tcw_xy_displacements <- merge(
#'    unique(testcase_tracklist[ , c("treatment", "trackid")]),
#'                        xy_displacements_for_testcase,
#'                        by = "trackid",
#'                        all = TRUE)
#'
#'  plot_as_density_x_start_end_distance_of_tracks_per_treatment(tcw_xy_displacements)
plot_as_density_y_start_end_distance_of_tracks_per_treatment <- function(table_w_treatment_trackid_xy_displacements){

  ggplot(table_w_treatment_trackid_xy_displacements,
         aes_string(x = "y.position",
                    group = "treatment",
                    fill = "as.factor(treatment)")) +
    geom_density(position = "identity", alpha=0.5) +
    scale_fill_discrete(name = "treatment") +
    theme_bw() +
    xlab("values") + ggtitle("y position start end displacement")

}
