#' attach move along class color to tracktable
#'
#' @param track_table a track table with coords and move.class
#' @param displacement_class_along_flow_columns the columns containing .move.class info (e.g. x and y)
#'
#' @return track table with attached movement class color
#' @export
#'
attach_move_along_class_color_to_tracktable <- function(track_table, displacement_class_along_flow_columns = c("x.position.move.class", "y.position.move.class")){
  class_color <- NA
  for(index in 1:dim(track_table)[1]){
    current_class_color <- move_along_classed_as_color(track_table[index, displacement_class_along_flow_columns])
    if(index == 1){
      class_color <- current_class_color
    }else{
      class_color <- rbind(class_color, current_class_color)
    }

  }
  answer_table <- data.frame(track_table, class_color)
  return(answer_table)
}
