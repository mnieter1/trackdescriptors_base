#' cluster the descriptor vectors specified
#'
#' @aliases hcluster
#' @param description_vector_per_trackid a matrix containing vectors containing
#'   the descriptions of a track columnwise
#' @return a hclust result
#' @export
#' @examples
#' hcluster_descriptors(
#'    description_vector_per_trackid = test_descriptor_with100divisions)
hcluster_descriptors <- function (
  description_vector_per_trackid){

  # clustering start ------------------------------------------

  #hclust of tracks
  # prepare hierarchical cluster
  hc = hclust(dist(t(as.matrix(description_vector_per_trackid)),method="manhattan"), method = "ward.D") #  "ward.D", "ward.D2", "single", "complete", "average" (= UPGMA),
  # "mcquitty" (= WPGMA), "median" (= WPGMC) or "centroid" (= UPGMC).


  return(hc)

}
