#' move along classed as color
#'
#' returns colors to use for plotting as movement classes
#' "red" for along flow
#' "blue" against flow
#' "green" as stationary
#'
#' @param displacement_class_along_flow
#'
#' @return a color encoding relative movement class
#' @export
#'
#' @examples
#' move_along_classed_as_color(c(-1,-1))
#' move_along_classed_as_color(c(2,0))
#' move_along_classed_as_color(c(0, 1))
move_along_classed_as_color <- function(displacement_class_along_flow){
  # if there is a 2 it goes with the flow
  if(sum(abs(displacement_class_along_flow) == 2) > 0){
    color2use <- "red"
  }else if(sum(abs(displacement_class_along_flow) == 0) > 0){# its against the stream
    color2use <- "blue"
  }else if(sum(abs(displacement_class_along_flow) == 1) == length(displacement_class_along_flow)){# if both are 1 , no movement
    color2use <- "green"
  }
  return(color2use)
}
