#' plot one colored tracks picture per condition
#'
#' @param coordinates_list identifiers for separating the coordinates into tracks
#' @param track_id_list identifiers for separating the coordinates into tracks
#'  annotating the \code{coordinates_list}
#' @param cluster_ids a list with the cluster_id for each track position
#'  annotating the \code{coordinates_list}
#' @param conditions_list a list with the differents conditions
#'  annotating the \code{coordinates_list}
#' @param mark_start_end a boolean stating if start and endpoints should be marked
#'
#' @return as many plots as there are different conditions
#'  found in the \code{conditions_list}
#' @export
#'
#' @examples
#' # first get the track cluster annotation
#' # annotate the longer coordinate list by annotating the track id list
#' # from the track annotation with the cluster ids from the hclust
#' testcase_trackids_with_clusterids_for_6clusters <-
#'    track_ids_with_cluster_ids_attached <- attach_cluster_ids_to_track_ids(
#'        track_ids = testcase_tracklist[ , "trackid"],
#'        hclust_result = example_hclust_result_for_autocorrelation_descriptor,
#'        number_of_clusters = 6)
#' # then depict the clusters as color info
#' plot_one_colored_tracks_picture_per_treatment(
#'    coordinates_list = testcase_tracklist[ , c("x.position", "y.position")],
#'    track_id_list = testcase_tracklist[ , "trackid"],
#'    conditions_list = testcase_tracklist[ , "treatment"],
#'    cluster_ids = as.character(
#'    testcase_trackids_with_clusterids_for_6clusters[ , "clusterid"]))
#'
#'
#' # example with just the DMSO subset creates single plot
#' plot_one_colored_tracks_picture_per_treatment(
#'      coordinates_list = DMSO_control_tracks[ , c("x.position", "y.position")],
#'      track_id_list = DMSO_control_tracks[ , "trackid"],
#'      conditions_list = DMSO_control_tracks[ , "treatment"],
#'      cluster_ids = DMSO_track_ids_with_cluster_ids_attached[ , "clusterid"],
#'      mark_start_end = TRUE)

plot_one_colored_tracks_picture_per_treatment <- function (coordinates_list, track_id_list, cluster_ids, conditions_list, mark_start_end = TRUE) {

  # figure out how many clusters there where
  nr_of_colors_to_use = length(unique(cluster_ids))
  # pass the above in the plot functions below to get correct color
  # representation even if some clusters are not populated

  # for each condition make a single plot
  conditions_are <- unique(conditions_list)
  nr_of_conditions <- length(conditions_are)

  for(condition_index in 1:nr_of_conditions) {
    # make subset
    pointer_list_to_condition <- which(as.character(conditions_list) %in% as.character(conditions_are[condition_index]))

    # make the plot
    plot_tracks_with_cluster_color_from_hclust(
      coordinates_list = coordinates_list[pointer_list_to_condition, ],
      track_id_list = track_id_list[pointer_list_to_condition],
      cluster_ids = cluster_ids[pointer_list_to_condition],
      condition_name = conditions_are[condition_index],
      nr_of_clusters = nr_of_colors_to_use,
      mark_start_end = mark_start_end)

  }


}
