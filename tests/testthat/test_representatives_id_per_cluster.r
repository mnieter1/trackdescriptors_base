context("cluster representative id")

test_that("example representatives for 15 clusters", {

  # take the reference descriptor and perform hierachical clusting
  hc_dev = hclust(dist(t(test_descriptor_with100divisions), method = "manhattan"))

  # calling the cluster representative function
  test_results_for_representative_names_15_clusters <- representatives_id_per_cluster(hcclust_result = hc_dev, description_vector_per_trackid = test_descriptor_with100divisions, number_of_clusters = 15)

  # loading the reference data set expicitely
  loaded_reference <- reference_results_for_representative_names_15_clusters

  #check if table has same length
  expect_equal(length(loaded_reference[ , "representative.id"]), 15)
  #check if table has same length
  expect_equal(length(test_results_for_representative_names_15_clusters[ , "representative.id"]), 15)

  # check if the names of the representatives are the same
  for( index in 1:length(loaded_reference[ , "representative.id"])) {
    # check the exact position by using the cluster id column

    ref_in <- loaded_reference[which(loaded_reference[ , "group.id"] == index) , "representative.id"]
  test_in <-test_results_for_representative_names_15_clusters[which(test_results_for_representative_names_15_clusters[ , "group.id"] == index) , "representative.id"]


    expect_equal(as.character(test_in) , as.character(ref_in))
  }

})
