context("member distribution count")

test_that("example member counts for six clusters", {

# calling the cluster distribution function on the reference track data input
# old version test_output_for_6_clusters <- member_counts_per_cluster(coordinate_list = testcase_tracklist[, c("x.position", "y.position")], track_ids = testcase_tracklist[, "trackid"], treatment_class = testcase_tracklist[, "treatment"], number_of_clusters = 6, nr_of_divisions = 100)

test_output_for_6_clusters <- member_counts_per_cluster(
  track_ids_w_treatment_class = unique(
    testcase_tracklist[ , c("treatment", "trackid")]),
  number_of_clusters = 6, descriptors = test_descriptor_with100divisions)

# make sure reference has the same column order as newly created input
# fixes an issue with the loaded data in the check environment
# having other column order as they have been created and saved previously
ref_ordered <- six_cluster_distribution_testcase_out[ , colnames(test_output_for_6_clusters)]


# calculate abs difference of the two tables, should be zero if identical
#checksum <- sum(colSums(abs(as.matrix(test_output_for_6_clusters) - as.matrix(six_cluster_distribution_testcase_out))))

checksum <- sum(colSums(abs(as.matrix(test_output_for_6_clusters) - as.matrix(ref_ordered))))

expect_equal(checksum, 0)

})
