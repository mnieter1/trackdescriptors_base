context("traveled distance per track")

test_that("traveled distance per track function", {

  #set up the testset the pathh should be 4, because for steps each one unit along x with constant y
  test_coordinates <- cbind(c(1,2,3,4,5), c(1,1,1,1,1))
  test_track_ids <- c(4,4,4,4,4)

  # 4 steps with one unit
  expect_equal(traveled_distance_per_track(coordinates_list = test_coordinates, track_id_list = test_track_ids)[ , "pathlength"], 4)

  #set up the testset the path should be 6, because four steps each one unit along x with constant y then two along y
  test_coordinates <- cbind(c(1,2,3,4,5,5,5), c(1,1,1,1,1,2,3))
  test_track_ids <- c(6,6,6,6,6,6,6)

  # 4 steps with one unit
  expect_equal(traveled_distance_per_track(coordinates_list = test_coordinates, track_id_list = test_track_ids)[ , "pathlength"], 6)


})
