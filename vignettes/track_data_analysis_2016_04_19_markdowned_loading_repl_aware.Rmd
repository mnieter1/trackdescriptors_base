---
title: "track analysis using trackdescriptors package"
author: "Manuel M. Nietert"
date: "`r Sys.Date()`"
output: rmarkdown::html_vignette
fig_caption: yes                    
vignette: >
  %\VignetteIndexEntry{track descriptors}
  %\VignetteEngine{knitr::rmarkdown}
  %\VignetteEncoding{UTF-8}
---

# load the package
```{r loading_package, echo=TRUE}
library(trackdescriptors)
```

# load the data set 
```{r loading_data, echo=TRUE}
#trackdata <- read.table(file = "dataset_from_knime_20160407_repl_aware.csv", sep = "\t", header = TRUE)
trackdata <- testcase_tracklist
```


# check the global dimension ranges
```{r checking_dimension_ranges, echo=TRUE}
x_range <- min(trackdata[ , "x.position"])
x_range[2] <- max(trackdata[ , "x.position"])
y_range <- min(trackdata[ , "y.position"])
y_range[2] <- max(trackdata[ , "y.position"])
```



# derive the classical track descriptor: path length, max displacement, start end displacement (x and y separate)


## x and y start end displacements
```{r xy_displacements, echo=TRUE, fig.width=7, fig.height=7}
# derive the displacement for x and y dimension
xy_displacements_for_trackdata <- displacement_vector_per_track(
  coordinates_list = trackdata[ , c("x.position", "y.position")],
  track_id_list = trackdata[ , "trackid"])

# merge the displacements with the treatment info
trackdata_xy_displacements <- merge(unique(trackdata[ , c("treatment", "trackid")]),
                              xy_displacements_for_trackdata,
                              by = "trackid",
                              all = T)
# first plot x displacement densities
plot_as_density_x_start_end_distance_of_tracks_per_treatment(trackdata_xy_displacements)
# then plot y displacement densities
plot_as_density_y_start_end_distance_of_tracks_per_treatment(trackdata_xy_displacements)
```



## max displacements
```{r max_displacements, echo=TRUE, fig.width=7, fig.height=7}
# derive the max displacement from start of track
max_displacement_for_trackdata <- max_displacement_from_start_per_track(
  coordinates_list = trackdata[ , c("x.position", "y.position")],
  track_id_list = trackdata[ , "trackid"])

# merge the displacements with the treatment info
trackdata_maxdisplacement <- merge(unique(trackdata[ , c("treatment", "trackid")]),
                    max_displacement_for_trackdata,
                    by = "trackid",
                    all = T)

plot_as_density_max_displacement_per_treatment(trackdata_maxdisplacement)
```


# check for the max displacement of a track to use for later scaling plots of single tracks
```{r checking_global_max_track_displacement, echo=TRUE}
abs_max_track_distance <- max(trackdata_maxdisplacement[ , "maxdisplacement"])
```


**max distance from start for a track** is **`r abs_max_track_distance`**

## path length a.k.a. the traveled distance per track

```{r path_length, echo=TRUE, fig.width=7, fig.height=7}
traveled_distances_for_trackdata <- traveled_distance_per_track(
  coordinates_list = trackdata[ , c("x.position", "y.position")],
  track_id_list = trackdata[ , "trackid"])

trackdata_pathlength <- merge(unique(trackdata[ , c("treatment", "trackid")]),
                           traveled_distances_for_trackdata,
                           by = "trackid",
                           all = T)

plot_as_density_path_lengths_of_track_per_treatment(trackdata_pathlength)
```


# calculate an autocorrelation vector representation per track
```{r calculate_autocorrelationdescriptor, echo=TRUE, fig.width=7, fig.height=7}
trackdata_autocorr100er <- autocorrelation_vector_per_trackid(
  coordinate_list = trackdata[ , c("x.position", "y.position")],
  track_ids = trackdata[ , "trackid"],
  nr_of_divisions = 100,
  distance_measure = "manhattan")
```


# nr of data points in the treatment sets
````{r, message=FALSE, eval=TRUE, echo=FALSE}
library(knitr)
nr_of_points_per_treatment <- table(trackdata[ , "treatment"])
header <- names(nr_of_points_per_treatment)
nr_of_points_per_treatment <- matrix(data = nr_of_points_per_treatment, nrow = 1, ncol = length(nr_of_points_per_treatment))
colnames(nr_of_points_per_treatment) <- header
kable(nr_of_points_per_treatment, format = "markdown")
````


# make a list with unique pairs of trackid and treatment
```{r make_unique_trackid_treatmentpair_list, echo=TRUE}
list_w_unique_trackids_n_treatments <- unique(trackdata[ , c("trackid", "treatment")])
```


# nr of tracks in the treatment sets
````{r, message=FALSE, eval=TRUE, echo=FALSE}
library(knitr)
nr_of_tracks_per_treatment <- table(list_w_unique_trackids_n_treatments[ , "treatment"])
header <- names(nr_of_tracks_per_treatment)
nr_of_tracks_per_treatment <- matrix(data = nr_of_tracks_per_treatment, nrow = 1, ncol = length(nr_of_tracks_per_treatment))
colnames(nr_of_tracks_per_treatment) <- header
kable(nr_of_tracks_per_treatment, format = "markdown")
````


# the histogram of step numbers per track for the population
```{r distribution_of_nr_of_track_steps_obeservations, echo=TRUE, fig.width=7, fig.height=7}
# make and plot histogram
hist(table(trackdata$trackid),
     main = "distribution for count of steps per track",
     col = "darkgrey",
     xlab = "nr of steps")
# annotate the number of singeltons, a.k.a. single points means actually no track
mtext(paste("nr of singeltons =", sum(table(trackdata$trackid) == 1)), 3, line=.5, adj=1)
```


## nr of singeltons
The number of singeltons in the population is **`r sum(table(trackdata$trackid) == 1)`**.

# take a look using heatmaps

## rows sorted by treatment
```{r heatmap_tracks_sorted_per_treatment, echo=TRUE, fig.width=7, fig.height=7}
heatmap_descriptors_and_tracks_unsorted(
  descriptor_table = t(trackdata_autocorr100er),
  treatment_ids = list_w_unique_trackids_n_treatments[ , "treatment"])
```


## rows sorted by track simmilarity
```{r heatmap_tracks_sorted_per_similarity_of_tracks, echo=TRUE, fig.width=7, fig.height=7}
heatmap_descriptors_unsorted_and_tracks_sorted(
  descriptor_table = t(trackdata_autocorr100er),
  treatment_ids = list_w_unique_trackids_n_treatments[ , "treatment"])
```



# take the descriptor and calculate the similarity tree, plot representatives
```{r cluster_tree_with_representatives, echo=TRUE, fig.width=7, fig.height=7, warning=FALSE}
plot_cluster_colored_tree_and_representative_tracks_and_fingerprints(
  track_coordinate_list = trackdata[ , c("x.position", "y.position")],
  track_id_list = trackdata[ , "trackid"],
  description_vector_per_trackid = trackdata_autocorr100er,
  number_of_clusters = 6)
```


## count the classes per treatment
```{r cluster_tree_member_count_per_treatment, echo=TRUE, fig.width=7, fig.height=7, warning=FALSE}
trackdata_6_cluster_treatment_distri <- member_counts_per_cluster(
  track_ids_w_treatment_class = list_w_unique_trackids_n_treatments,
  number_of_clusters = 6,
  trackdata_autocorr100er)
```


## show the data table with counts
```{r show_cluster_tree_member_count_per_treatment, echo=TRUE, fig.width=7, fig.height=7, warning=FALSE}
show_cluster_distribution_per_condition(trackdata_6_cluster_treatment_distri)
```


## now make it percentage per treatment
```{r cluster_tree_member_count_per_treatment_percentaged, echo=TRUE, fig.width=7, fig.height=7, warning=FALSE}
trackdata_6_cluster_treatment_distri_percentaged <- columnwise_percentaged(trackdata_6_cluster_treatment_distri)
```


## show the data table with counts now as relative percentages per treatment 
```{r show_cluster_tree_member_count_per_treatment_percentaged, echo=TRUE, fig.width=7, fig.height=7, warning=FALSE}
show_cluster_distribution_per_condition(trackdata_6_cluster_treatment_distri_percentaged)
```


# make a cluster tree object using the descriptors
```{r take_descriptor_and_hcluster, echo=TRUE, fig.width=7, fig.height=7, warning=FALSE}
trackdata_6_cluster_tree <- hcluster_descriptors(trackdata_autocorr100er)
```

# take the cluster object and annotate the cluster ids derived by specifying number of branches to be cut
```{r take_hclust_result_and_aannotate_branch_id_to_trajectories, echo=TRUE, fig.width=7, fig.height=7, warning=FALSE}
trackdata_w_6_clusters <- attach_cluster_ids_to_trajectories(
  trajectory_table = trackdata,
  hclust_result = trackdata_6_cluster_tree,
  number_of_clusters = 6)
```



# plot the tracks colored by clusterid one per treatment
```{r plot_one_colored_tracks_picture_per_treatment, echo=TRUE, fig.width=7, fig.height=7, warning=FALSE}
plot_one_colored_tracks_picture_per_treatment(
  coordinates_list = trackdata_w_6_clusters[ , c("x.position", "y.position")],
  track_id_list = trackdata_w_6_clusters[ , "trackid"],
  cluster_ids = trackdata_w_6_clusters[ , "clusterid"],
  conditions_list = trackdata_w_6_clusters[ , "treatment"],
  mark_start_end = TRUE)
```


# plot a picture of all tracks provided into one hairball depiction; a.k.a. all tracks start at origin
```{r plot_hairball_of_all_tracks, echo=TRUE, fig.width=7, fig.height=7, warning=FALSE}
plot_all_tracks_transposed_w_start_to_origin(
  coordinates_list = trackdata_w_6_clusters[ , c("x.position", "y.position")],
  track_id_list = trackdata_w_6_clusters[ , "trackid"],
  main_title = "all tracks",
  outreach = abs_max_track_distance)
```


# plot a picture of all tracks per treatment provided as one hairball depiction; a.k.a. all tracks start at origin
```{r plot_hairball_of_tracks_per_treatment, echo=TRUE, fig.width=7, fig.height=7, warning=FALSE}
plot_one_hairball_tracks_picture_per_treatment(
  coordinates_list = trackdata_w_6_clusters[ , c("x.position", "y.position")],
  track_id_list = trackdata_w_6_clusters[ , "trackid"],
  conditions_list = trackdata_w_6_clusters[ , "treatment"],
  outreach = abs_max_track_distance)
```


# plot a picture of all tracks per treatment as KDE depiction with side histograms; aka all tracks start at origin
```{r plot_one_kde_centered_tracks_picture_per_treatment, echo=TRUE, fig.width=7, fig.height=7, warning=FALSE}
plot_one_kde_centered_tracks_picture_per_treatment(
  coordinates_list = trackdata_w_6_clusters[ , c("x.position", "y.position")],
  track_id_list = trackdata_w_6_clusters[ , "trackid"],
  conditions_list = trackdata_w_6_clusters[ , "treatment"])
```


# plot a picture of all track endpoints per treatment as KDE depiction with side histograms; a.k.a. all tracks start at origin and this shows where the ends were
```{r plot_one_kde_centered_tracks_picture_per_treatment_just_endpoints, echo=TRUE, fig.width=7, fig.height=7, warning=FALSE}
plot_one_kde_centered_tracks_picture_per_treatment_use_just_endpoint_coords(
  coordinates_list = trackdata_w_6_clusters[ , c("x.position", "y.position")],
  track_id_list = trackdata_w_6_clusters[ , "trackid"],
  conditions_list = trackdata_w_6_clusters[ , "treatment"])
```


# plot a picture of all track endpoints per treatment as KDE depiction with side histograms; a.k.a. all tracks start at origin and this shows where the ends were
```{r plot_one_kde_centered_tracks_picture_per_treatment_just_endpoints_w_lims, echo=TRUE, fig.width=7, fig.height=7, warning=FALSE}
plot_one_kde_centered_tracks_pic_per_treatment_use_just_endpoints_w_lims(
  coordinates_list = trackdata_w_6_clusters[ , c("x.position", "y.position")],
  track_id_list = trackdata_w_6_clusters[ , "trackid"],
  conditions_list = trackdata_w_6_clusters[ , "treatment"],
  lims = c(-1*abs_max_track_distance,
           abs_max_track_distance,
           -1*abs_max_track_distance,
           abs_max_track_distance))
```

# plot the vector fields of mean track start end displacements per patch 
```{r plot_one_colored_tracks_picture_per_treatment_w_vector_field, echo=TRUE, fig.width=7, fig.height=7, warning=FALSE}
# define parameters
# nr of zones in x
nr_of_x_zones <- 10
# nr of zones in y
nr_of_y_zones <- 10
# the limits of the zone window
lims_to_use <- c(0, 2000, 0, 2000)

plot_one_colored_tracks_picture_per_treatment_w_vector_field(
  coordinates_list = trackdata_w_6_clusters[ , c("x.position", "y.position")],
  track_id_list = trackdata_w_6_clusters[ , "trackid"],
  cluster_ids = trackdata_w_6_clusters[ , "clusterid"],
  conditions_list = trackdata_w_6_clusters[ , "treatment"],
  mark_start_end = TRUE,
  nr_of_x_zones = nr_of_x_zones,
  nr_of_y_zones = nr_of_y_zones,
  lims_to_use = lims_to_use,
  arrow_color = "black",
  arrow_lwd = 4)

```
